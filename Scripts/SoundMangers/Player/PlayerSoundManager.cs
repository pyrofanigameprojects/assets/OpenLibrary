﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundManager : MonoBehaviour
{
    public List<AudioClip> audios;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void GotHit()
    {
        WhatToPlay("Hit");
    }
    public void MininingSound()
    {
        WhatToPlay("Mining");
    }
    private void WhatToPlay(string audiosName)
    {
        foreach(AudioClip audioClip in audios)
        {
            if (audioClip.name.Contains(audiosName))
            {
                audioSource.clip = audioClip;
                audioSource.Play();
            }
        }

    }

}
