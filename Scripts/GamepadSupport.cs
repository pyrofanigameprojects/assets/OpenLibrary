﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GamepadSupport : MonoBehaviour
{
    public KeyCode actionKey,rotateLeft,rotateRight;
    public List<GameObject> doozyViews;
    // Start is called before the first frame update
    void Start()
    {

        keycodeAssigner();
    }

    // Update is called once per frame
    void Update()
    {
        keycodeAssigner();
    }
    void keycodeAssigner()
    {
        if (Input.GetJoystickNames().Length > 0)
        {
            actionKey = KeyCode.Joystick1Button0;
            rotateLeft = KeyCode.Joystick1Button4;
            rotateRight = KeyCode.Joystick1Button5;
        }
        else
        {
            actionKey = KeyCode.Mouse0;
            rotateLeft = KeyCode.Q;
            rotateRight = KeyCode.E;
        }
    }
}
