﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationsScript : MonoBehaviour
{
    //[HideInInspector]
    //private int stateIndex;
    [HideInInspector]
    public int directionIndex;
    private Animator enemyAnimator;
    private SpriteRenderer[] spritesOfEnemy;
    private Transform transformToChangeScale;
    private Vector3 initialScale;
    private EnemyStates enemyStates;
    // Start is called before the first frame update
    void Awake()
    {
        spritesOfEnemy = GetComponentsInChildren<SpriteRenderer>(true);
        enemyAnimator = GetComponent<Animator>();
        transformToChangeScale = GetComponent<Transform>();
        initialScale = transformToChangeScale.localScale;
        enemyStates = GetComponentInParent<EnemyStates>();
        enemyStates.onEnemyDirectionChange += EnemyDirectionHasChanged;
        enemyStates.onEnemyStateChange += EnemyStateChanged;



    }

    void EnemyDirectionHasChanged(EnemyStates.enemyDirection direction)
    {
        //Debug.Log(direction.ToString());
        directionIndex = (int)direction;
        TurnLeft((int)direction);
        enemyAnimator.SetInteger("Direction", directionIndex);
    }


    void EnemyStateChanged(EnemyStates.enemyState enemyState)
    {
        switch (enemyState)
        {
            case EnemyStates.enemyState.Patrolling:
                enemyAnimator.SetBool("Idle", false);
                enemyAnimator.SetBool("Walking", true);
                break;
            case EnemyStates.enemyState.Chasing:
                enemyAnimator.SetBool("Walking", true);
                enemyAnimator.SetBool("Idle", false);
                break;
            case EnemyStates.enemyState.GetAway:
                enemyAnimator.SetBool("Walking", true);
                enemyAnimator.SetBool("Idle", false);
                break;
            case EnemyStates.enemyState.Attacking:
                enemyAnimator.SetTrigger("Attack");
                break;
            case EnemyStates.enemyState.Idling:
                enemyAnimator.SetBool("Walking", false);
                enemyAnimator.SetBool("Idle", true);
                break;
            case EnemyStates.enemyState.Hitted:
                enemyAnimator.SetTrigger("Hit");
                StartCoroutine(ChangeColourEffect());
                break;
            case EnemyStates.enemyState.Death:
                enemyAnimator.SetTrigger("Death");
                break;
        }

    }




    // Update is called once per frame
    void Update()
    {
        
    }

    private void TurnLeft(int directionindex)
    {
       
        if (directionindex.Equals(3))
        {
            transform.localScale = new Vector3(-initialScale.x, initialScale.y, initialScale.z);
        }
        else
        {
            transform.localScale = new Vector3(initialScale.x, initialScale.y, initialScale.z);
        }

    }
    public void GotHitAnimation()
    {
        StartCoroutine(ChangeColourEffect());
        enemyAnimator.SetTrigger("Hit");
    }
    IEnumerator ChangeColourEffect()
    {
        ChangeColour(spritesOfEnemy, Color.red);
        yield return new WaitForSeconds(1);
        ChangeColour(spritesOfEnemy, Color.white);

    }
    private void ChangeColour(SpriteRenderer[] spritesToChangeColour,Color wantedColor)
    {
        foreach(SpriteRenderer sprite in spritesToChangeColour)
        {
            sprite.material.SetColor("_ColorBlood",wantedColor);
        }
    }

}
