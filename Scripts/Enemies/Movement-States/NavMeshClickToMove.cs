﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshClickToMove : MonoBehaviour
{


    [SerializeField] NavMeshAgent agent;

    [SerializeField] float distanceToGetAway = 5f;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    //Dirty Hack to wait until calculation of pathfinding is finished
    IEnumerator GetPathRemainingDistance(NavMeshAgent navMeshAgent)
    {

        if (navMeshAgent.pathStatus == NavMeshPathStatus.PathInvalid || navMeshAgent.path.corners.Length == 0) yield return 0;
        yield return new WaitUntil(() => !navMeshAgent.pathPending);

        float distance = 0.0f;
        for (int i = 0; i < navMeshAgent.path.corners.Length - 1; ++i)
        {
            distance += Vector3.Distance(navMeshAgent.path.corners[i], navMeshAgent.path.corners[i + 1]);
        }

        if (distance < distanceToGetAway)
        {
            Debug.Log(distance);
        }
        else
        {
            Debug.LogWarning("GotAway");
            navMeshAgent.isStopped = true; //stops agent
            navMeshAgent.velocity = Vector3.zero; //instant stop, remove it to have a slow stop
        }
    }

}
