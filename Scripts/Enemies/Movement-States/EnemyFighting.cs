﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFighting : MonoBehaviour
{
    //If Enemy is close to player Attack Else Patrol

    [SerializeField] GameObject player;
    private bool isAttacking = false;

    [SerializeField] float timeOfEveryAttack;

    private void OnEnable()
    {
        isAttacking = false;
        gameObject.GetComponent<EnemyStates>().onEnemyStateChange += EnemyStateHasChanged;
    }

    void EnemyStateHasChanged(EnemyStates.enemyState state)
    {
        if (state == EnemyStates.enemyState.Attacking)
        {

            StartAttacking();
        }
        else
        {

            StopAttacking();
        }

    }

    void StartAttacking() {

        isAttacking = true;
        InvokeRepeating("TakeDamage", timeOfEveryAttack, timeOfEveryAttack);

    }

    void StopAttacking() {
        isAttacking = false;
        CancelInvoke("DoDamage");
    }

    private void DoDamage() {
        //if player is Alive
        if (player != null)
        {


        }
        else { 
        
        }

    
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
