﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Patrol))]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyStates : MonoBehaviour
{
    public enum enemyState { Idling,Patrolling, Chasing, Attacking, Hitted, Death,GetAway };
    public enum enemyDirection { Down, Up, Right, Left  };


    private enemyDirection currentDirection;
    private Patrol patrol;
    private EnemyChase chase;
    private NavMeshAgent agent;

    public delegate void EnemyStateChange(enemyState state);
    public delegate void EnemyDirectionChange(enemyDirection direction);

    public event EnemyStateChange onEnemyStateChange;
    public event EnemyDirectionChange onEnemyDirectionChange;


    float timer;

    public DayNightManager dayNightManager;
    [SerializeField]
    private float behaviourRandomizedFloat;


    public void changeState(enemyState state)
    {
        //onEnemyStateChange(state);//δυμιουργουσε θεμα μετα ανιμεισον στην αρχη καθως χωρις στεΐτ δν παιζουν
                                  //Cheking if anyone subscribed to our event
        if (onEnemyStateChange != null)
        {
            //    //publish event
            onEnemyStateChange(state);
    }
}

    private void changeDirection(enemyDirection direction) {
        //Cheking if anyone subscribed to our event
        if (onEnemyDirectionChange != null) {
            //publish event
            onEnemyDirectionChange(direction);
        }
    }


    public void UpdateState(enemyState state)
    {
        //switch (state)
        //{
        //    case enemyState.Patrolling:
        //        patrol.startPatrolling();
        //        break;
        //    case enemyState.Chasing:
        //        patrol.stopPatrolling();
        //        break;
        //    case enemyState.Attacking:
        //        patrol.stopPatrolling();
        //        break;
        //    case enemyState.Idling:
        //        patrol.stopPatrolling();
        //        break;
        //    case enemyState.Hitted:
        //        patrol.stopPatrolling();
        //        break;
        //    case enemyState.Death:
        //        patrol.stopPatrolling();
        //        break;
        //}

    }
    private void Awake()
    {
        dayNightManager = GameObject.FindObjectOfType<DayNightManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        StartCoroutine(InitializeState());
        behaviourRandomizedFloat = Random.Range(50, dayNightManager.DayNightVariables.maxSecsPerDay);
        
    }
    private void OnEnable()
    {
        changeState(enemyState.Patrolling);
    }

    // Update is called once per frame
    void Update()
    {  
        if (timer >= behaviourRandomizedFloat)
        {
            changeState(enemyState.Idling);
            timer = 0;
        }
    }

    private void FixedUpdate()
    {
        calculateDirectionChange();
    }







    private void calculateDirectionChange()
    {

        Vector3 normalizedMovement = agent.desiredVelocity.normalized;
        Vector3 forwardVector = Vector3.Project(normalizedMovement, transform.forward);
        Vector3 rightVector = Vector3.Project(normalizedMovement, transform.right);

        float forwardVelocity = forwardVector.magnitude * Vector3.Dot(forwardVector, transform.forward);
        float rightVelocity = rightVector.magnitude * Vector3.Dot(rightVector, transform.right);

        if (Mathf.Abs(rightVelocity) >= Mathf.Abs(forwardVelocity))
        {
            if (rightVelocity >= 0)
            {
                //Moving Right
                if (currentDirection != enemyDirection.Right)
                {
                    currentDirection = enemyDirection.Right;
                    changeDirection(currentDirection);
                }

            }
            else if (rightVelocity <= 0)
            {
                //Moving Left
                if (currentDirection != enemyDirection.Left)
                {
                    currentDirection = enemyDirection.Left;
                    changeDirection(currentDirection);
                }
            }
        }
        else
        {
            if (forwardVelocity >= 0)
            {
                //Moving Up
                if (currentDirection != enemyDirection.Up)
                {
                    currentDirection = enemyDirection.Up;
                    changeDirection(currentDirection);
                }
            }
            else if (forwardVelocity <= 0)
            {
                //Moving Down
                if (currentDirection != enemyDirection.Down)
                {
                    currentDirection = enemyDirection.Down;
                    changeDirection(currentDirection);
                }
            }
        }
    }
    private IEnumerator InitializeState()
    {
        yield return new WaitForSeconds(5);
        ChangeStateRandom();
    }
    public void ChangeStateRandom()
    {
        EnemyStates.enemyState state = (EnemyStates.enemyState) Random.Range(0, 2);
        //onEnemyStateChange(state);
        changeState(state);
    }

}
