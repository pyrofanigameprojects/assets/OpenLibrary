﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
public class Patrol : MonoBehaviour
{

    public Transform[] points;
    private int destPoint = 0;
    private UnityEngine.AI.NavMeshAgent agent;

    private bool isPatrolling=false;


    private void OnEnable()
    {

        gameObject.GetComponent<EnemyStates>().onEnemyStateChange += EnemyStateHasChanged;
    }

    void EnemyStateHasChanged(EnemyStates.enemyState state) {
        if (state == EnemyStates.enemyState.Patrolling) {
            //Patrol
            StartPatrolling();
        }
        else if(state == EnemyStates.enemyState.Idling) {
            //Idling
            StartIdling();
        }
        else
        {
            //Stop Patrol
            StopPatrolling();
        }
    
    }

    void StartIdling() {

        agent.isStopped = true;
        agent.autoBraking = false;
        agent.updateRotation = false;
        isPatrolling = false;
    }

    void Start()
    {
        agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (points.Length == 1)
        {
            //play Idle Animation
            gameObject.GetComponent<EnemyStates>().changeState(EnemyStates.enemyState.Idling);

        }

    }


    void GotoNextPoint()
    {
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }


    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (agent.remainingDistance < 0.3f && isPatrolling && points.Length == 1) {
            //play Idle Animation
            gameObject.GetComponent<EnemyStates>().changeState(EnemyStates.enemyState.Idling);

        }
        else if (!agent.pathPending && agent.remainingDistance < 0.5f && isPatrolling)
        {
            GotoNextPoint();
        }
    }

    public void StopPatrolling() {
        isPatrolling = false;
        

    }

    public void StartPatrolling() {
        agent.isStopped = false;
        agent.autoBraking = false;
        agent.updateRotation = false;
        isPatrolling = true;
        


        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        

        
    }

}
