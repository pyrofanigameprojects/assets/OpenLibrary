﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyChase : MonoBehaviour
{

    //[SerializeField] Camera mainCam;

    NavMeshAgent agent;

    [SerializeField] float distanceToGetAway = 24f;

    GameObject player;

    [SerializeField] float maxDistanceToAttack = 1.5f;

    [SerializeField] float timeOfEveryAttack;

    EnemyStates.enemyState currentState;

    private bool isChassing = false;
    private bool isAttacking = false;
    private bool isGettinngAway = false;


    float timer = 0;
    float attackTime;

    DayNightManager dayNightManager;
    EnemyStates enemyStates;
    EnemyHealthManager healthManager;
    EnemyAnimationsScript enemyAnimations;

    public bool toxic;
    private Vector3 locationTOGetAway;

    private Color defaultColor = Color.white;

    private void OnEnable()
    {

        //gameObject.GetComponent<EnemyStates>().onEnemyStateChange += EnemyStateHasChanged;
    }

    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        attackTime = 3 / agent.speed;
        enemyStates = GetComponent<EnemyStates>();
        dayNightManager = GameObject.FindObjectOfType<DayNightManager>();
        player = GameObject.FindObjectOfType<MovementOvewright>().gameObject;
        healthManager = GetComponent<EnemyHealthManager>();
        enemyAnimations = GetComponentInChildren<EnemyAnimationsScript>();
        enemyStates.onEnemyStateChange += EnemyStateHasChanged;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        GetAway();

    }

    void EnemyStateHasChanged(EnemyStates.enemyState state)
    {
        currentState = state;
        if (state == EnemyStates.enemyState.Chasing)
        {
            StartChasing();
        }
        else
        {

            StopChasing();
        }
        if (state == EnemyStates.enemyState.Attacking)
        {
            StartAttackState();
        }
        else
        {
            StopAttacking();
        }
        if (state == EnemyStates.enemyState.Hitted)
        {
            if (dayNightManager.nightfall)
            {
                StartChasing();
            }
            else
            {
                StartGettingAway();
            }
        }
        if (state == EnemyStates.enemyState.GetAway)
        {
            StartGettingAway();
        }
        else
        {
            StopGettingAway();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("playerBolt") || other.CompareTag("LaikaBolt"))
        {
            if (dayNightManager.nightfall)
            {
                enemyStates.changeState(EnemyStates.enemyState.Chasing);
            }
            else
            {
                enemyStates.changeState(EnemyStates.enemyState.GetAway);
            }
            enemyAnimations.GotHitAnimation(defaultColor);
        }
        if (other.CompareTag("Player"))
        {
            other.GetComponentInParent<CharStatsManager>().health -= GetComponent<EnemyHealthManager>().health / 4;
            other.GetComponentInParent<CharactersAnimationsAndSfxManager>().InteractOnHit(Color.white);
            enemyStates.changeState(EnemyStates.enemyState.Attacking);
        }
        if (other.CompareTag("DoDamageOvertime") && !toxic)
        {
            InsideToxicPools();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (timer >= attackTime)
            {
                other.GetComponentInParent<CharStatsManager>().health -= GetComponent<EnemyHealthManager>().health / 4;
                other.GetComponentInParent<CharactersAnimationsAndSfxManager>().InteractOnHit(Color.white);
                enemyStates.changeState(EnemyStates.enemyState.Attacking);
                timer = 0;
            }

        }
        if (other.CompareTag("DoDamageOvertime"))
        {
            if (!toxic)
            {
                InsideToxicPools();
            }
            else
            {
                if (healthManager.health <= 300)
                {
                    healthManager.health += 1;
                }
                defaultColor = Color.magenta;
                PowerUpEffectPlay(defaultColor);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("DoDamageOvertime") && !toxic)
        {
            OutsideToxicPools();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        isGettinngAway = false;
    }

    void StartChasing()
    {

        agent.SetDestination(player.transform.position);
        agent.isStopped = false;
        agent.autoBraking = true;
        agent.updateRotation = false;
        isChassing = true;

    }
    void StartGettingAway()
    {
        isGettinngAway = true;
        agent.isStopped = false;
        agent.autoBraking = true;
        agent.autoRepath = true;
        agent.updateRotation = false;
        if (locationTOGetAway.Equals(Vector3.zero))
        {
            locationTOGetAway = transform.position + transform.right.normalized * 5;
            agent.SetDestination(locationTOGetAway);
        }



    }
    void StopGettingAway()
    {
        isGettinngAway = false;
        agent.autoRepath = false;
        locationTOGetAway = Vector3.zero;


    }
    void StopChasing()
    {
        isChassing = false;

    }


    //Invoke
    void DoDamageToPlayer()
    {
        enemyStates.changeState(EnemyStates.enemyState.Attacking);
    }





    private void FixedUpdate()
    {

        ChaseDistanceCalculatorAndStateChange();
        Attack();

    }
    private void StartAttackState()
    {
        isAttacking = true;
    }
    private void StopAttacking()
    {
        isAttacking = false;
    }
    private void Attack()
    {
        if (!agent.pathPending && isAttacking)
        {
            float distance = 0.0f;
            for (int i = 0; i < agent.path.corners.Length - 1; ++i)
            {
                distance += Vector3.Distance(agent.path.corners[i], agent.path.corners[i + 1]);
            }
            if (distance <= maxDistanceToAttack && dayNightManager.nightfall)
            {
                agent.SetDestination(player.transform.position);
                if (currentState != EnemyStates.enemyState.Attacking)
                {
                    enemyStates.changeState(EnemyStates.enemyState.Attacking);
                    InvokeRepeating("DoDamageToPlayer", timeOfEveryAttack, timeOfEveryAttack);

                }

            }
            else
            {
                enemyStates.changeState(EnemyStates.enemyState.GetAway);
            }
        }
    }
    private void ChaseDistanceCalculatorAndStateChange()
    {
        if (dayNightManager.nightfall)
        {
            ChaseWithDistance();
        }
        if (!agent.pathPending && isChassing)
        {
            float distance = 0.0f;
            for (int i = 0; i < agent.path.corners.Length - 1; ++i)
            {
                distance += Vector3.Distance(agent.path.corners[i], agent.path.corners[i + 1]);
            }
            if (distance < distanceToGetAway)
            {

                CancelInvoke("DoDamageToPlayer");
                agent.SetDestination(player.transform.position);
                enemyStates.changeState(EnemyStates.enemyState.Chasing);

            }

            else
            {
                //Call Something to Determine patrolling or Idling maybe?
                StopChasing();
                enemyStates.changeState(EnemyStates.enemyState.Patrolling);

            }

        }

    }
    private void ChaseWithDistance()
    {
        float distanceBettweenPlayerAndEnemy = Vector3.Distance(transform.position, player.transform.position);
        if (distanceBettweenPlayerAndEnemy <= 5)
        {
            enemyStates.changeState(EnemyStates.enemyState.Chasing);
        }
    }
    private void GetAway()
    {
        if (isGettinngAway)
        {


            if (agent.remainingDistance <= 0.3f)
            {
                StopGettingAway();
            }
        }
        else if (currentState.Equals(EnemyStates.enemyState.GetAway))
        {
            enemyStates.changeState(EnemyStates.enemyState.Idling);
        }


    }
    private void InsideToxicPools()
    {
        enemyStates.changeState(EnemyStates.enemyState.GetAway);
        if (timer >= 1.4f)
        {
            enemyAnimations.GotHitAnimation(Color.green);
            timer = 0;

        }
    }
    private void OutsideToxicPools()
    {
        StartCoroutine(enemyAnimations.ResetColourWithDelay());
    }
    private void PowerUpEffectPlay(Color poweredUpColor)
    {
        enemyStates.changeState(EnemyStates.enemyState.Patrolling);
        if (timer >= 1.4f)
        {
            timer = 0;
            StartCoroutine(enemyAnimations.ChangeColourEffectPowerUp(Color.white, poweredUpColor));

        }
    }


}