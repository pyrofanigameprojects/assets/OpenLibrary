﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHealthManager : MonoBehaviour
{
    [Range(1, 200)]
   public int health;

    [SerializeField]
    [Range(1, 10)]
    int despawnTime;

    public void ReceiveDamage(int damage)
    {

        health -= damage;
        if (health <= 0)
        {
            gameObject.GetComponent<EnemyStates>().changeState(EnemyStates.enemyState.Death);
            StartCoroutine(Despawn());
            DisableAll();
        }
    }

    IEnumerator Despawn() { 
        yield return new WaitForSeconds(despawnTime);
        Destroy(gameObject.transform.parent.gameObject);


    }
    private void DisableAll()
    {
        GetComponent<EnemyChase>().enabled = false;
        GetComponent<Patrol>().enabled = false;
        GetComponent<EnemyStates>().enabled = false;
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponentInChildren<EnemyAnimationsScript>().enabled = false;
        Collider[] collider = GetComponentsInChildren<Collider>();
        foreach(Collider coli in collider)
        {
            coli.enabled = false;
        }
    }
}
