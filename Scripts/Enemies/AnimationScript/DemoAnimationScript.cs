﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoAnimationScript : MonoBehaviour
{

    private void OnEnable()
    {
    
            gameObject.GetComponent<EnemyStates>().onEnemyDirectionChange += DoAnimation;
    }

    void DoAnimation(EnemyStates.enemyDirection direction) {
            //Debug.Log((int)direction);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
