﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingButtons : MonoBehaviour
{
    private InventoryVariables inventoryVariables;
    [Header("Costs Of Upgrades")]
    [SerializeField]
    private int wood;
    [SerializeField]
    private int crystal;
    [SerializeField]
    private int ore;
    [SerializeField]
    private int fuel;
    private Button craftButton;
    private CraftingManager craftingManager;
    // Start is called before the first frame update
    void Start()
    {
        craftButton = GetComponent<Button>();
        craftingManager = GetComponentInParent<CraftingManager>();
        craftButton.interactable = false;
    }
    private void OnEnable()
    {
        inventoryVariables = craftingManager.inventoryVariables;

    }
    // Update is called once per frame
    void Update()
    {
        if (CheckIfHaveEnougeFuelAndOre() && CheckIfHaveEnougeWoodAndCrystals())
        {
            craftButton.interactable = true;
        }
        else
        {
            craftButton.interactable = false;

        }
    }

    public void OnMouseEnter()
    {
        craftingManager.wood = wood;
        craftingManager.crystals = crystal;
        craftingManager.fuel = fuel;
        craftingManager.ore = ore;
    }
    public void OnMouseExit()
    {
        craftingManager.wood = 0;
        craftingManager.crystals = 0;
        craftingManager.fuel = 0;
        craftingManager.ore = 0;
    }
    private bool CheckIfHaveEnougeWoodAndCrystals()
    {
         return wood <= inventoryVariables.wood && crystal <= inventoryVariables.crystals;
    } 
    private bool CheckIfHaveEnougeFuelAndOre()
    {
         return fuel <= inventoryVariables.fuel && ore <= inventoryVariables.metal;
    }
}
