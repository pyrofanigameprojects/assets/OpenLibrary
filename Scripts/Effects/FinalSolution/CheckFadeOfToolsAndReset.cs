﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckFadeOfToolsAndReset : MonoBehaviour
{
    private Material[] axeMaterial, pickMaterial,gunMaterials;//assign materials
    private SelectDeselectButton selectDeselect;
    private ToolEffectManager tool;
    private SeperateMaterials seperateMaterials;
    private float fadeAxe, fadePick,fadeGun;
    private void Awake()
    {

        seperateMaterials = GetComponent<SeperateMaterials>();



    }
    // Start is called before the first frame update
    void Start()
    {
        axeMaterial = seperateMaterials.axeMaterials.ToArray();
        pickMaterial = seperateMaterials.pickMaterials.ToArray();
        gunMaterials = seperateMaterials.gunMaterials.ToArray();
        selectDeselect = GameObject.FindObjectOfType<SelectDeselectButton>();
        tool = GetComponent<ToolEffectManager>();

    }
    void FixedUpdate()
    {
        GiveSignals();
    }
    // Update is called once per frame
    void Update()
    {   
    }
    private void GiveSignals()
    {
        if (selectDeselect.activatedMainButton)
        {
            if (fadeGun <= 1)
            {
                fadeGun += Time.deltaTime;
            }
            tool.EffectManager(gunMaterials, true, false, fadeGun);

        }
        else
        {
            if (fadeGun >= 0)
            {
                fadeGun -= Time.deltaTime;

            }
            tool.EffectManager(gunMaterials, false, true, fadeGun);
        }
        if (selectDeselect.activatedAxeButton)
        {
            if (fadeAxe <= 1)
            {
                fadeAxe += Time.deltaTime;
            }
            tool.EffectManager(axeMaterial,true,false, fadeAxe);
        }
        else
        {
            if (fadeAxe >= 0)
            {
                fadeAxe -= Time.deltaTime;

            }
            tool.EffectManager(axeMaterial, false, true, fadeAxe);

        }

        if (selectDeselect.activatedPickButton)
        {
            if (fadePick <= 1)
            {
                fadePick += Time.deltaTime;
            }
            tool.EffectManager(pickMaterial, true, false, fadePick);
        }
        else {
            if (fadePick >= 0)
            {
                fadePick -= Time.deltaTime;
            }
            tool.EffectManager(pickMaterial, false, true, fadePick);
        }
    }


}
