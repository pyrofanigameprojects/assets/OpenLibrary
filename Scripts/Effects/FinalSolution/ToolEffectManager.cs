﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CheckFadeOfToolsAndReset),typeof(SeperateMaterials))]
public class ToolEffectManager : MonoBehaviour
{
    private Color initialColor;
    // Start is called before the first frame update
    void Start()
    {
        initialColor = new Color(0,1, 0.4784314f,1);

    }

    // Update is called once per frame
    void Update()
    {   
    
    }

    
    public void EffectManager(Material[] material,bool fadeIn,bool disolve,float fade)
    {  
            BasicEffectMethod(material, fadeIn, disolve,fade);

        
    }
    private void BasicEffectMethod(Material[] material, bool fadeIn, bool disolve, float fade)
    {
        if (fadeIn)
        {
            SetFadeToDesiredIfFadeInExecuted(material, fade, initialColor);
            if (fade >= 1)
            {
                SetFadeToDesiredIfFadeInExecuted(material, fade, new Color(0, 0, 0));
            }
        }
        if (disolve)
        {
            SetFadeToDesiredIfFadeInExecuted(material, fade, initialColor);
            if (fade <= 0)
            {
                SetFadeToDesiredIfFadeInExecuted(material, fade, initialColor);
            }
        }

    }
    private void SetFadeToDesiredIfFadeInExecuted(Material[] materials, float fadeAmount, Color desiredColor)
    {   foreach(Material material in materials)
        {
            material.SetFloat("_Fade", fadeAmount);
            material.SetColor("_HologramColor", desiredColor);
        }
        

    }



}
