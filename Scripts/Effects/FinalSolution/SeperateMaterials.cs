﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SeperateMaterials : MonoBehaviour
{
    private SeperationAndCollectionOfCharactersSprites seperationAndCollection;
    private GameObject[] axeObjects, pickObjects,gunObjects;
    [HideInInspector]
    public List<Material> axeMaterials, pickMaterials,gunMaterials;
    // Start is called before the first frame update
    void Start()
    {
        seperationAndCollection = GetComponent<SeperationAndCollectionOfCharactersSprites>();
        axeObjects = seperationAndCollection.axesAll.ToArray();
        pickObjects = seperationAndCollection.picksAll.ToArray();
        gunObjects = seperationAndCollection.gunAll.ToArray();
        FindMaterials(axeObjects, axeMaterials);
        FindMaterials(pickObjects, pickMaterials);
        FindMaterials(gunObjects, gunMaterials);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FindMaterials(GameObject[] arrayToFind,List<Material> listToCreate)
    {
        foreach (GameObject equipment in arrayToFind)
        {
            listToCreate.Add(equipment.GetComponent<SpriteRenderer>().material);
        }
    }
}
