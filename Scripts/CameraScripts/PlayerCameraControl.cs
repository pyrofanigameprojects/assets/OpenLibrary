﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraControl : MonoBehaviour
{
    private float rotationY=0;
    [Tooltip("dont starve divides 360 by 8 thats why it is 45 you can minimize it if you want but in division of 360 also much be not greater of 90 you can easily get lost that way")]
    public float degreesToRotate=0;
    private new Transform transform;
    //attach to player sprite Prefab
    // Start is called before the first frame update
    void Start()
    {
        transform = this.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        cameraRotate();
    }
    private void cameraRotate()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            rotationY =rotationY-degreesToRotate;
        }
         if (Input.GetKeyDown(KeyCode.E))
        {
            rotationY = rotationY+degreesToRotate;
        }
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, rotationY, 0);
    }
}
