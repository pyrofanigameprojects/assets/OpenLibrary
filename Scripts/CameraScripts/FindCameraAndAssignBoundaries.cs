﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FindCameraAndAssignBoundaries : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<CinemachineConfiner>().m_BoundingVolume = this.GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
