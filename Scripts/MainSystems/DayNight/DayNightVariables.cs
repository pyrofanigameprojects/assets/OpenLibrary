﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DayCycle", menuName = "DayNightCycles", order = 1)]
public class DayNightVariables : ScriptableObject
    
{
    [Range(0,1.5f)]
    [Tooltip("max intensity")]
    public float  brightness=0;
    [Range(0.1f,1)]
    [Tooltip("the rate in which it changes daylightState")]
    public float   rateOfDaylightChange=0;
    [Range(350, 700)]
    [Tooltip("maximum amount of seconds for the day to change")]
    public float  maxSecsPerDay=0;
    [Range(120,280)]
    [Tooltip("higher numbers mean longer starting day if max its summer")]
    public float startingDayDuration;
    [Range(10,30)]
    [Tooltip("the rate at which season changes higher numbers mean   faster rate of season change")]
    public float sunLightDecreament = 0;
    [Tooltip("the day we are currently on ")]
    public int currentDay = 0;

}
