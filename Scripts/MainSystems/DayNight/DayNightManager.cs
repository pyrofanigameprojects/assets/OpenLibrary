﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightManager : MonoBehaviour
{
    private float timer=0;//public mono gia debugg
    //[HideInInspector]
    public bool sunrise=false,nightfall=false,dusk=false;//xrisimopiite apo to dayNightIndicator
    public DayNightVariables DayNightVariables;
    [HideInInspector]
    public float dayDurationIndicator;//xrisimopiite apo to dayNightIndicator
    [HideInInspector]
    public List<Light> naturalLights;
    [HideInInspector]
    public List<Light> baseLights;
    [SerializeField]
    private int currentDayOfSeason;
    private void Awake()
    {
        dayTimeManager();
        currentDayOfSeason = 1;
    }
    // Start is called before the first frame update
    void Start()
    {
        DayNightVariables.brightness = 1.5f;
        DayNightVariables.rateOfDaylightChange = 0.4f;
        DayNightVariables.currentDay=1;
        foreach (Light light in Resources.FindObjectsOfTypeAll<Light>())
        {
            if (light.CompareTag("NaturalLights")&&light!=null)
            {
                naturalLights.Add(light);
            }
             if (light != null &&light.CompareTag("BaseLights"))
            {
                baseLights.Add(light);
            }
        }
        StartCoroutine(DaySequencer()); 
    }
    // Update is called once per frame
    void Update()
    {   
        timer += Time.deltaTime;
        dayTimeManager();
        ScenelightsManager();
    }

    private void ChangeLightsIntensity(Light light,bool increase)//in order to increase minvalue and max value are reversed
    {
        if (increase&& light.intensity<=DayNightVariables.brightness)
        {   
                light.intensity += DayNightVariables.rateOfDaylightChange * Time.deltaTime;
        }
        else
        {
                light.intensity -= DayNightVariables.rateOfDaylightChange * Time.deltaTime;
        }
  
    }
    private void ScenelightsManager()
    {
        if (sunrise)
        {
            foreach (Light light in naturalLights)
            {
                ChangeLightsIntensity(light, true);
            }
            foreach (Light light in baseLights)
            {
                ChangeLightsIntensity(light, false);
            }
        }
        if (dusk)
        {
            foreach (Light light in naturalLights)
            {   if (light.intensity >= DayNightVariables.brightness/2)
                {
                    ChangeLightsIntensity(light, false);
                }
            }
        }
        if (nightfall)
        {
            foreach (Light light in naturalLights)
            {
                ChangeLightsIntensity(light, false);
            }
            foreach (Light light in baseLights)
            {
                ChangeLightsIntensity(light, true);
            }
        }
        
    }
    private void DayDurationManager()
    {
        float maximumDayDurtation = DayNightVariables.maxSecsPerDay * 0.8f;
        float minimumDayDuration = DayNightVariables.maxSecsPerDay * 0.6f;
        float value = currentDayOfSeason * DayNightVariables.sunLightDecreament;
        dayDurationIndicator = PingPong(value, minimumDayDuration, maximumDayDurtation);
        SeasonChangerManager(value, maximumDayDurtation);
    }
    private void dayTimeManager()
    {
        //at summer in our hemisphere night takes around 0.4 or 40% of a persons day if you consider that is 8 hours of sleep and an exntra 2 hours wwhile awake
        DayDurationManager();
        if (timer >= DayNightVariables.maxSecsPerDay)//this plain and simple. mostly a counter and reseter of days
        {
            timer = 0;
            DayNightVariables.currentDay++;
            currentDayOfSeason++;
            StartCoroutine(DaySequencer());
        }
    }
    private void SeasonChangerManager(float increment,float maxSunlightHours)
    {
      
        if (increment >= maxSunlightHours)
        {
            currentDayOfSeason = 1;

        }
    }
    float PingPong(float value,float min,float max)
    {
       return Mathf.PingPong(value, max - min) + min;
    }

    IEnumerator DaySequencer()
    {
        sunrise = true;
        nightfall = false;
        yield return new WaitForSeconds(dayDurationIndicator / 2);
        sunrise = false;
        dusk = true;
        yield return new WaitForSeconds(dayDurationIndicator / 2);
        dusk = false;
        nightfall = true;
    }
}
