﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveBrightNessAdjuster : MonoBehaviour
{
    private Collider[] colliders;
    public List<Collider> entranceCollider, exitCollliders;
    public DayNightVariables DayNightVariables;
    private float initialBrightness;
    private float initialRateOfSunlight;
    // Start is called before the first frame update
    void Start()
    {
        colliders = GetComponentsInChildren<Collider>();
        initialBrightness = DayNightVariables.brightness;
        initialRateOfSunlight = DayNightVariables.rateOfDaylightChange;
        seperateColliders(colliders, entranceCollider, exitCollliders);
    }
    private void FixedUpdate()
    {
    }
    // Update is called once per frame
    void Update()
    {
        checkWhatCollided();
    }
    private void checkWhatCollided()
    {
        foreach(Collider collider in entranceCollider)
        {
            if (collider.GetComponent<CaveCollidersScript>().collided)
            {
                DayNightVariables.brightness = 0.4f;
                DayNightVariables.rateOfDaylightChange = initialRateOfSunlight*2;

            }
        }   
        foreach(Collider collider in exitCollliders)
        {
            if (collider.GetComponent<CaveCollidersScript>().collided)
            {
                DayNightVariables.brightness = 1.5f;
                DayNightVariables.rateOfDaylightChange = initialRateOfSunlight;

            }
        }
    }
    private void seperateColliders(Collider[] colliders,List<Collider> entrance, List<Collider> exit)
    {
        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.name.Contains("Entrance"))
            {
                entrance.Add(collider);
            } 
            if (collider.gameObject.name.Contains("Exit"))
            {
                exit.Add(collider);
            }
        }
    }

}
