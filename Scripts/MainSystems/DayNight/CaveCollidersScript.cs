﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveCollidersScript : MonoBehaviour
{
    //[HideInInspector]
    public bool collided;
    public List<Collider> listToModify;
    // Start is called before the first frame update
    void Start()
    {
     if (gameObject.name.Contains("Entrance"))
        {
            listToModify = GetComponentInParent<CaveBrightNessAdjuster>().exitCollliders;
        }
        else
        {
            listToModify = GetComponentInParent<CaveBrightNessAdjuster>().entranceCollider;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            collided = true;
            SetBollInstance(listToModify, false);
        }
    }
    private void SetBollInstance(List<Collider> colliders, bool whatToDo)
    {
        foreach (Collider collider in colliders)
        {
            collider.GetComponent<CaveCollidersScript>().collided = whatToDo;
        }
    }
}
