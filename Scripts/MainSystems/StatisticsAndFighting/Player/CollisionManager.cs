﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager : MonoBehaviour
{
    [HideInInspector]
    public bool outsideBase = false;//used by pop up event
    [HideInInspector]
    public bool insideCraftsColliders,doDamageOvertime;
    public string[] enemyTags;
    private CharactersAnimationsAndSfxManager charactersEffects;
    // Start is called before the first frame update
    void Start()
    {
        charactersEffects = GetComponent<CharactersAnimationsAndSfxManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        //interactionsWithEnemy(other);
        if (other.gameObject.CompareTag("Crafting"))
        {
            insideCraftsColliders = true;

        }
        if (other.gameObject.CompareTag("Base"))
        {
            outsideBase = false;
            StartCoroutine(charactersEffects.animationsPlayer.ResetColourWithDelay());
        }
        if (other.gameObject.CompareTag("DoDamageOvertime"))
        {
            doDamageOvertime = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Base"))
        {
            outsideBase = false;

           
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Crafting"))
        {
            insideCraftsColliders = false;
        }
        if (other.gameObject.CompareTag("Base"))
        {
           outsideBase = true;
        }
        if (other.gameObject.CompareTag("DoDamageOvertime"))
        {
            doDamageOvertime = false;
            StartCoroutine(charactersEffects.animationsPlayer.ResetColourWithDelay());
        }
    }
    //public void interactionsWithEnemy(Collider collider)
    //{
    //    foreach (string enemyTag in enemyTags)
    //    {
    //        if (collider.CompareTag(enemyTag))
    //        {
    //            charactersEffects.InteractOnHit();
    //        }
    //    }
    //}
}
