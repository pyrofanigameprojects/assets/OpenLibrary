﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.UI;
[RequireComponent(typeof(CollisionManager),typeof(PlayerUiInteractions),typeof(CharactersAnimationsAndSfxManager))]
[RequireComponent(typeof(RefillBullets))]
public class CharStatsManager : MonoBehaviour
{
    public CharStats charstats;
    private MovementOvewright movementOvewright;
    private CollisionManager collisionManager;
    private CharactersAnimationsAndSfxManager charactersEffects;
    [HideInInspector]
    public float health, oxygen;
    [HideInInspector]
    public float maxHealth, maxOxygen;
    public  float timerDmgOvertime;
    private PlayerUiInteractions playerUi;
    private int i;

    // Start is called before the first frame update
    void Start()
    {
        collisionManager = GetComponent<CollisionManager>();
        charactersEffects = GetComponent<CharactersAnimationsAndSfxManager>();
        movementOvewright = GetComponent<MovementOvewright>();
        playerUi = GetComponent<PlayerUiInteractions>();
        movementOvewright.speed = charstats.speed;
        maxHealth = charstats.health;
        health = maxHealth;
        maxOxygen = charstats.oxygen;
        oxygen = maxOxygen;
    }
    void FixedUpdate()
    {
        Die();
        DoDamageOverTimer(collisionManager.doDamageOvertime);
        BaseInteractionsStatisticalManager();

    }
    // Update is called once per frame
    void Update()
    {
        movementOvewright.StopCharacterMovement(charstats.speed);
        timerDmgOvertime += Time.deltaTime;
    }
    private void DoDamageOverTimer(bool doDamage)
    {
        if (doDamage)
        {
            health -= 20* Time.deltaTime;
           charactersEffects.InteractionWithDelay(1,Color.green);


        }

    }
    private void BaseInteractionsStatisticalManager()
    {
        if (charstats._NeedsOxygen)
        {
            if (collisionManager.outsideBase && oxygen >= 0)
            {
                oxygen -=0.5f* Time.deltaTime;
            }
            else
            {
                if (maxOxygen >= oxygen)
                {
                    oxygen +=8*Time.deltaTime;
                }
                if (maxHealth >= health)
                {
                    health +=8* Time.deltaTime;
                }
            }
            if (oxygen <= 1)
            {
                health -= Time.deltaTime;
                charactersEffects.InteractionWithDelay(2.5f,new Color(0.341f, 0.784f, 1));
            }
        }
        else if (maxHealth >= health)
        {
            health +=0.3f* Time.deltaTime;
        }
    }
    private void Die()
    {
        if (health <= 0)
        {
            charactersEffects.DeathEffects();
            StartCoroutine(disableThisGameObject());
            playerUi.winningLosePanel.Lost();
            health=0.5f;

        }
    }
    private void DisableAll()
    {   GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GetComponentInChildren<AnimationsPlayer>().enabled = false;
        GetComponent<MovementOvewright>().enabled = false;
        GetComponent<RefillBullets>().enabled = false;
        GetComponent<CombatSystem>().enabled = false;
        GetComponent<inventoryManager>().enabled = false;
        GetComponent<AudioSource>().enabled = false;
        GetComponent<CharactersAnimationsAndSfxManager>().enabled = false;
        GetComponent<CollisionManager>().enabled = false;
    }
    IEnumerator disableThisGameObject()
    {
        DisableAll();
        Collider[] collider = GetComponentsInChildren<Collider>();
        foreach(Collider coli in collider)
        {
            coli.enabled = false;
        }
        yield return new WaitForSeconds(1);
        enabled = false;
    }

}
