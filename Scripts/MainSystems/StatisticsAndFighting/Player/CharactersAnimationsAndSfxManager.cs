﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersAnimationsAndSfxManager : MonoBehaviour
{   [HideInInspector]
    public AnimationsPlayer animationsPlayer;
    private PlayerSoundManager playerSound;
    private CharStatsManager charStatsManager;

    // Start is called before the first frame update
    void Start()
    {
        playerSound = GetComponent<PlayerSoundManager>();
        animationsPlayer = GetComponentInChildren<AnimationsPlayer>();
        charStatsManager = GetComponent<CharStatsManager>();

    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void InteractOnHit(Color colorToReturnTo)
    {
        playerSound.GotHit();
        animationsPlayer.HitAnimationsPlay(colorToReturnTo);
    }
    public void DeathEffects()
    {
        animationsPlayer.Death();
    }
    public void InteractionWithDelay(float delay, Color colorToReturnTo)
    {
        if (charStatsManager.timerDmgOvertime>= delay)
        {
            InteractOnHit(colorToReturnTo);
            charStatsManager.timerDmgOvertime = 0;
        }
    }

}
