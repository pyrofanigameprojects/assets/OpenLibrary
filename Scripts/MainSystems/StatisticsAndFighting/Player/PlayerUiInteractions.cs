﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUiInteractions : MonoBehaviour
{   
    [HideInInspector]
    public WinningLosePanel winningLosePanel;
    // Start is called before the first frame update
    void Start()
    {
        winningLosePanel = GameObject.FindObjectOfType<WinningLosePanel>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
