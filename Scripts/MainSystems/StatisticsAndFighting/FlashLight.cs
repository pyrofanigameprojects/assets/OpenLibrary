﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class FlashLight : MonoBehaviour
{
    private Light[] playersFlashLight;
    private bool pressed;
    private GameObject nightVision;
    // Start is called before the first frame update
    void Start()
    {
        playersFlashLight = GetComponentsInChildren<Light>();
        LightEnabler(playersFlashLight, false);
        if (GetComponentInChildren<Volume>() != null)
        {
            nightVision = GetComponentInChildren<Volume>().gameObject;
        }
    }
    private void FixedUpdate()
    {

    }
    // Update is called once per frame
    void Update()
    {
        FlashLightController();
        FlashInputManager();
    }
    private void FlashLightController()
    {
        if (pressed)
        {
            LightEnabler(playersFlashLight, true);
            nightVisionEnable(true);

        }
        else
        {
            nightVisionEnable(false);
            LightEnabler(playersFlashLight, false);
        }
    }
    private void FlashInputManager()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            pressed = !pressed;
        }
    }
    private void LightEnabler(Light[] lights,bool enable)
    {
        foreach (Light light in lights)
        {
            light.enabled = enable;
        }
    }
    private void nightVisionEnable(bool enable)
    {
        if (nightVision != null)
        {
            nightVision.SetActive(enable);
        }
    }
}
