﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatsManager : MonoBehaviour
{
    public CharStats charStats;
    public string[] playerTags;//ta tags p tou kanoun zimia
    [HideInInspector]
    public float health, speed,dmg;
    private float timer,reloadTime;
    // Start is called before the first frame update
    void Start()
    {
        health = charStats.health;
        speed = charStats.speed;
        dmg = charStats.dmg;
        reloadTime = charStats.reloadTime;

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        DoDMG(other);
        interactionsWithPlayer(other);
    }
    private void OnTriggerStay(Collider collider)
    {
        if (timer >= reloadTime)
        {
            DoDMG(collider);
            timer = 0;
        }
    }
    private void interactionsWithPlayer(Collider collider)
    {   
        foreach (string playerTag in playerTags)
            {
                if (collider.CompareTag(playerTag))
                {//εδω μονο ανιμεισονς την ζημια την παθαινει απο αλου
                 //health -= collider.gameObject.GetComponent<EnemyStatsManager>().dmg;
                InteractOnHitEnemy();
                }
            }

    }
    private void DoDMG(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            if (timer >= reloadTime)
            {   //add attackTrigger
                collider.gameObject.GetComponent<CharStatsManager>().health -= dmg;
                collider.gameObject.GetComponent<CharactersAnimationsAndSfxManager>().InteractOnHit(Color.white);
            }
        }
    }
    private void InteractOnHitEnemy()
    {
        StartCoroutine(colorChanger());
        //animeison
        //hxos
    }
    IEnumerator colorChanger()//kalitera na to baleis sto enemyAnimationsScript
    {
        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>(true);
        foreach (SpriteRenderer sprite in sprites)
        {
            sprite.material.SetColor("_ColorBlood", Color.red);
        }
        yield return new WaitForEndOfFrame();
        foreach(SpriteRenderer spriteRenderer in sprites)
        {
            spriteRenderer.material.SetColor("_ColorBlood", Color.white);
           
        }
    }


}
