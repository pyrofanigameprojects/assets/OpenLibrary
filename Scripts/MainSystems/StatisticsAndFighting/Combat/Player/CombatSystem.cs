﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatSystem : MonoBehaviour
{
    public Transform boltSpawnPoint; //temporary fix for jam
    private  Vector3 charactersInputs;
    private GameObject boltPrefab;
    public float timer;
    private CharStatsManager charStatsManager;
    [HideInInspector]
    public Vector3 boltDirection;
    private Vector3 charactersDirection;
    private SelectDeselectButton SelectDeselectButton;
    private Vector3 boltRotation;
    private AnimationsPlayer animationsPlayer;
    public Vector3 mousePosition;
    public Vector3 mousePositionToWorld;
    // Start is called before the first frame update
    void Start()
    {
        SelectDeselectButton = GameObject.FindObjectOfType<SelectDeselectButton>();
        charStatsManager =GetComponent<CharStatsManager>();
        boltPrefab = GetComponent<CharStatsManager>().charstats.bolt;
        animationsPlayer = GetComponentInChildren<AnimationsPlayer>();
        

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        Combat();
        
        orientationOrganizer();
        BoltRotationSetter();
        timer += Time.fixedDeltaTime;
    }
    private void Update()
    {
        
    }
    private void Combat()
    {   
        if (charStatsManager.charstats.isPlayer && Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (timer >= charStatsManager.charstats.reloadTime)
            {
                if (!charStatsManager.charstats.ranged)
                {
                    animationsPlayer.AttackAnimationPlay();
                    InstanciateBullets();
                    timer = 0;
                }
                if (charStatsManager.charstats.ranged &&SelectDeselectButton.activatedMainButton)
                {
                    if (GetComponent<inventoryManager>().inventoryVariables.ammo != 0)
                    {
                        animationsPlayer.AttackAnimationPlay();
                        InstanciateBullets();
                        GetComponent<inventoryManager>().inventoryVariables.ammo--;
                        timer = 0;
                    }
                }
            }

        }

    }
    private void InstanciateBullets()
    {
        Instantiate(boltPrefab, boltSpawnPoint.position, Quaternion.Euler(45,0,boltRotation.z));
    }
    private void orientationOrganizer()
    {
        charactersDirection = GetComponent<MovementOvewright>().charactersDirection;
        boltDirection = charactersDirection * charStatsManager.charstats.attackRange;

    }
    private void BoltRotationSetter()
    {
        charactersInputs.x = Input.GetAxisRaw("Horizontal");
        charactersInputs.z = Input.GetAxisRaw("Vertical");
        if (charactersInputs.z >= 0)
        {
            boltRotation.z=90;
        }
        if (charactersInputs.Equals(Vector3.zero))
        {
            boltRotation.z = 90;
        }
        if (charactersInputs.z<0)
        {
            boltRotation.z = -90;
        }
        if (charactersInputs.x < 0)
        {
            boltRotation.z = 180;
        }
        if (charactersInputs.x>0)
        {
            boltRotation.z = 0;
        }
    }
    private void enemyCombat()
    {

    }
}
