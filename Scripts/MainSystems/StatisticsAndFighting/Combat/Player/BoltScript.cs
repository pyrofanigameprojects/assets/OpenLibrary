﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltScript : MonoBehaviour
{
    private CharStatsManager charStatsManager;
    private Vector3 targetPosition,targetPositionIfNotMoving;
    private float boltSpeed;
    private GameObject playerObj;
    private Vector3 initialPosition;
    private float rangeDist,currentDistance;
    private int dmg;

    private float maxFlyingTime = 4f;
    private float timeCounter=0f;
    // Start is called before the first frame update
    void Start()
    {
        playerObj = GameObject.FindObjectOfType<CharStatsManager>().gameObject;
        charStatsManager = playerObj.GetComponent<CharStatsManager>();
        targetPosition = playerObj.GetComponent<CombatSystem>().boltDirection;
        boltSpeed = charStatsManager.charstats.boltSpeed;
        initialPosition = transform.position;
        rangeDist=charStatsManager.charstats.attackRange;
        dmg = Mathf.RoundToInt(charStatsManager.charstats.dmg);
        targetPositionIfNotMoving = playerObj.transform.forward.normalized * rangeDist;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (targetPosition == Vector3.zero) {
            transform.position = Vector3.MoveTowards(transform.position,transform.position+targetPositionIfNotMoving , boltSpeed * Time.deltaTime);
        }
        else transform.position = Vector3.MoveTowards(transform.position, transform.position + targetPosition, boltSpeed * Time.deltaTime);
        destoyBolt();
        timeCounter += Time.fixedDeltaTime;
        

    }
    private void destoyBolt()
    {
        if (timeCounter > maxFlyingTime) Destroy(this.gameObject);

        currentDistance = Vector3.Distance(initialPosition, transform.position);
        if (currentDistance>rangeDist)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("enemy"))
        {
            other.gameObject.GetComponent<EnemyHealthManager>().ReceiveDamage(dmg);
            Destroy(this.gameObject);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("enemy"))
        {
            Destroy(this.gameObject);
        }
    }
}
