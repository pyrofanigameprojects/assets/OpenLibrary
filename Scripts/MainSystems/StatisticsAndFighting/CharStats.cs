﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="characterStats",menuName ="CombatStatistics/CharStats",order =1)]
public class CharStats : ScriptableObject
{   [Range(0.2f,5)]
    public float reloadTime;
    [Range(0.5f,30)]
    public float attackRange;
    [Range(0, 200)]
    public float  health;
    [Range(0,120)]
    public float  oxygen;
    [Range(0.5f,4)]
    public float  speed;
    [Range(2.5f,120)]
    public float  dmg;
    [Range(6,24)]
    public float boltSpeed;
    public GameObject bolt;
    public bool ranged,isPlayer,_NeedsOxygen;
}
