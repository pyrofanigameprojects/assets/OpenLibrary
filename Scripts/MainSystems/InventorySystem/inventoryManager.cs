﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventoryManager : MonoBehaviour
{   //attach to player
    public InventoryVariables inventoryVariables;
    // Start is called before the first frame update
    void Start()
    {
        inventoryVariables.wood = 0;
        inventoryVariables.crystals = 0;
        inventoryVariables.fuel = 0;
        inventoryVariables.metal = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("wood"))
        {
            inventoryVariables.wood++;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("crystals"))
        {
            inventoryVariables.crystals++;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("fuel"))
        {
            inventoryVariables.fuel++;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("metal"))
        {
            inventoryVariables.metal++;
            Destroy(other.gameObject);
        }
    }
}
