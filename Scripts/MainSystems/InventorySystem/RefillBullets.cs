﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(inventoryManager))]
public class RefillBullets : MonoBehaviour
{   [SerializeField]
    private float refilTime=0.5f;
    private float timer;
    private inventoryManager inventoryManager;
    private InventoryVariables inventoryVariables;
    private CollisionManager collisionManagerPlayer;
    private int maxBulletes;
    // Start is called before the first frame update
    void Start()
    {
        inventoryManager = GetComponent<inventoryManager>();
        inventoryVariables = inventoryManager.inventoryVariables;
        maxBulletes = inventoryVariables.ammo;
        collisionManagerPlayer = GetComponent<CollisionManager>();
    }

    // Update is called once per frame
    void Update()
    {
        RefillingBullets(collisionManagerPlayer.outsideBase);
    }
    private void RefillingBullets(bool isOutsideBase)
    {
        timer += Time.deltaTime;
        if (SurpacedTimeAndHaveCanisters()&&!isOutsideBase)
        {
            inventoryVariables.ammo++;
            timer = 0;
        }
    }
    private bool SurpacedTimeAndHaveCanisters()
    {
        return timer >= refilTime && inventoryVariables.ammo < maxBulletes;
    }
}
