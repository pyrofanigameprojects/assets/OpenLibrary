﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="InventoryVariables",menuName ="Inventory/Variables",order =0)]
public class InventoryVariables : ScriptableObject
{
    public int wood, metal, fuel, crystals,ammo;
    public bool acuiredAxe, acuiredPickaxe;

}
