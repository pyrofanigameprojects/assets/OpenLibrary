﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="materialProperties",menuName = "Inventory/Material", order =2)]
public class FirstMaterialsProperties : ScriptableObject
{
    public int amountOfMaterial=1;
    public string materialsTag;
    public float timerToGather = 0;
}
