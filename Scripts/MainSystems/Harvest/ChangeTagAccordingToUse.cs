﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTagAccordingToUse : MonoBehaviour
{
    private SelectDeselectButton selectDeselect;
    private GameObject[] axes, picks,tools;
    // Start is called before the first frame update
    void Start()
    {

        selectDeselect = GameObject.FindObjectOfType<SelectDeselectButton>();
    }

    // Update is called once per frame
    void Update()
    {
        tagManager();
    }
    private void tagManager()
    {
     if (selectDeselect.activatedAxeButton)
        {
            tag = "Axe";
        }
         else  if (selectDeselect.activatedPickButton)
        {
            tag = "PickAxe";
        }
        else if (!selectDeselect.activatedMainButton)
        {
            tag = "Fists";
        }
     
    }
}
