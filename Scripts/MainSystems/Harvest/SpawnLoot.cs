﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLoot : MonoBehaviour
{

    [SerializeField]
    public List<GameObject> loot = new List<GameObject>();
    [SerializeField]
    [Range(1, 99)]
    public int minNumber=3;
    [SerializeField]
    [Range(2, 100)]
    private int maxNumber = 4;
    [SerializeField]
    private Transform spawnPoint;
    private bool hasBenCollected = false;

    [Header("Click To Spawn")]
    public bool spawnLoot = false;


    private void OnValidate()
    {
        if (minNumber > maxNumber) maxNumber++;
    }


    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnLoot && !hasBenCollected) {
            spawnLoot = false;
            Loot();
        }    
    }

    private void Loot() {
        hasBenCollected = true;
        int number = Random.Range(minNumber, maxNumber);
        StartCoroutine(CreateLoot(number));
    }

    IEnumerator CreateLoot(int number) {

        for (int i = 0; i < number; i++) {
            GameObject tempLoot = Instantiate(loot[Random.Range(0, loot.Count)]);
            tempLoot.transform.position = spawnPoint.position+new Vector3(0,1,0);
            yield return new WaitForSeconds(0.15f);
        
        }
    
    }







}
