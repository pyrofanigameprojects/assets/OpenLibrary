﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarvestableResourceManager : MonoBehaviour
{   [Header("MaxHealthToDeminish")]
    [SerializeField]
    public float maxHealth;
    [Header("TagOfMainObjectToInteract")]
    [SerializeField]
    private string mainTag;
    //[Header("TagOfSecondareyObject")]
    //[SerializeField]
    //private string secondaryTag;
    [Header("SpriteToDeactivate")]
    [SerializeField]
    private SpriteRenderer spriteToDisable;
    [Header("IfTrueTheOnHarvestDoesDmg")]
    [SerializeField]
    private bool _DoDmg;
    private Transform[] transormWithChildren;
    // Start is called before the first frame update
    void Start()
    {
        transormWithChildren = GetComponentsInChildren<Transform>();
        maxHealth = maxHealth * GetComponentInParent<Transform>().localScale.y;
    }
    private void FixedUpdate()
    {
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        ToolInteractions(other);
    }
    private void OnTriggerStay(Collider other)
    {
        //ToolInteractions(other);
    }
    private void OnTriggerExit(Collider other)
    {
        ToolInteractions(other);
    }
    private void ToolInteractions(Collider other)
    {   if (other.CompareTag("LaikaBolt"))
        {
            HealthChecker();
            StartCoroutine(InteractionShow());
            maxHealth -= 20;
        }
        if (other.CompareTag(mainTag))
        {
            WhatSfxToPlayAccordingToMainTag(other);
            HealthChecker();
            StartCoroutine(InteractionShow());
            maxHealth -= 10;
        }
        if(other.CompareTag("Fists")&!mainTag.Contains("Pick"))
        {
            DmgPlayer(_DoDmg, other);
            HealthChecker();
            StartCoroutine(InteractionShow());
            maxHealth -= 2.5f;
        }

    }
    private void HealthChecker()
    {
        if (maxHealth <= 0)
        {
            GetComponent<SpawnLoot>().spawnLoot = true;
            StopCoroutine(InteractionShow());
            if (transormWithChildren.Length==1)
            {
                StartCoroutine(DestoryIfNotParent());
            }
            else
            {
                StartCoroutine(SpriteDisabler());

            }
        }
    }
    private IEnumerator InteractionShow()
    {
        spriteToDisable.material.SetColor("_EffectColor", Color.yellow);
        GetComponentInParent<SpriteRenderer>().material.SetColor("_EffectColor", Color.yellow);
        yield return new WaitForSeconds(0.3f);
        spriteToDisable.material.SetColor("_EffectColor", Color.white);
        GetComponentInParent<SpriteRenderer>().material.SetColor("_EffectColor", Color.white);
    }
    private void DmgPlayer(bool doDmg,Collider other)
    {
        if (doDmg)
        {
            StartCoroutine(delayHitAnimation(other));
        }
    }
    private IEnumerator DestoryIfNotParent()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }
    //private IEnumerator Disabler()
    //{
    //    Collider[] coli = GetComponentsInChildren<Collider>();
    //    foreach (Collider col in coli)
    //    {
    //        col.enabled = false;
    //    }
    //    yield return new WaitForSeconds(0.1f);
    //    enabled = false;
      
    //}
    private IEnumerator SpriteDisabler()
    {
        spriteToDisable.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        enabled = false;
        //StartCoroutine(Disabler());

    }
    private IEnumerator delayHitAnimation(Collider other)
    {
        CharStatsManager charStatsManager = other.GetComponentInParent<CharStatsManager>();
        yield return new WaitForSeconds(0.2f);
        charStatsManager.health -= 5;
        charStatsManager.GetComponent<CharactersAnimationsAndSfxManager>().InteractOnHit(Color.white);
        
    }
    private void WhatSfxToPlayAccordingToMainTag(Collider other)
    {
        if (mainTag.Contains("Pick"))
        {
            other.GetComponentInParent<PlayerSoundManager>().MininingSound();
        }
    }
}
