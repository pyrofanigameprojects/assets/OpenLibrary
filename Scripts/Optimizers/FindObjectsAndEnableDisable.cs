﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindObjectsAndEnableDisable : MonoBehaviour
{  
    //[SerializeField]
    private HarvestableResourceManager[] harvestableResourceManagers;
    private DayNightManager dayNight;
    [SerializeField]
    private float distanceToShowUp;
    private float initialDistance;
    private GameObject player;
    //[SerializeField]
    private EnemyStates[] enemyStates;
    public UiData uiData;
    // Start is called before the first frame update
    private void Awake()
    {
        initialDistance = distanceToShowUp;
        harvestableResourceManagers = GameObject.FindObjectsOfType<HarvestableResourceManager>(true);
        enemyStates=GameObject.FindObjectsOfType<EnemyStates>(true);
        dayNight = GameObject.FindObjectOfType<DayNightManager>();
    }
    void Start()
    {
        player = GameObject.FindObjectOfType<MovementOvewright>().gameObject;
    }
    private void LateUpdate()
    {
        EnablerDisablerListHarvest(harvestableResourceManagers);
        EnablerDisablerListEnemies(enemyStates);
    }
    // Update is called once per frame
    void Update()
    {
        UpdateDistanceAccordingToFov();
    }
    private void UpdateDistanceAccordingToFov()
    {
        if (uiData.fov >= 80)
        {
            distanceToShowUp = 70;
        }
        else
        {
            distanceToShowUp = initialDistance;
        }
    }
    private void EnablerDisablerListHarvest(HarvestableResourceManager[] harvestables)
    {
        foreach (HarvestableResourceManager harvestable in harvestables)
        {   if (harvestable!=null)
            {
                distanceChecker(harvestable.gameObject);
            }


        }
    }  
    private void EnablerDisablerListEnemies(EnemyStates[] enemies)
    {
        foreach (EnemyStates enemie in enemies)
        {   if (enemie != null)
            {
                distanceChecker(enemie.gameObject);
            }
        }
    }
    private void distanceChecker(GameObject gameObject)
    {
        if (Vector3.Distance(player.transform.position, gameObject.transform.position) <= distanceToShowUp)
        {
            gameObject.SetActive(true);
        }
        else if (!dayNight.nightfall)
        {
            gameObject.SetActive(false);
        }
    }
}
