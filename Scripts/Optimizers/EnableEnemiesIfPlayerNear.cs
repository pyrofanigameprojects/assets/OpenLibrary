﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class EnableEnemiesIfPlayerNear : MonoBehaviour
{
    public GameObject ObjectsContainingEnemies;
    public float distanceToEnable=40f;
    private GameObject player;
    public float distance;
    // Start is called before the first frame update
    void Start()
    {
        distanceToEnable = 40;
        player = GameObject.FindObjectOfType<AnimationsPlayer>().gameObject;   
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, ObjectsContainingEnemies.transform.position);
        if (Vector3.Distance(player.transform.position, ObjectsContainingEnemies.transform.position) <= distanceToEnable)
        {
            ObjectsContainingEnemies.SetActive(true);
        }
        else
        {
            ObjectsContainingEnemies.SetActive(false);
        }
    }



}
