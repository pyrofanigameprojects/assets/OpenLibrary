﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.UI;
public class CraftingPanel : MonoBehaviour
{
    public UIView craftingPanel,repairMenu;
    private inventoryManager inventoryManager;
    private InventoryVariables inventoryVariables;
    private CollisionManager collisionManager;
    [HideInInspector]
    public bool pressed;
    // Start is called before the first frame update
    void Start()
    {
        inventoryManager = GameObject.FindObjectOfType<inventoryManager>();
        collisionManager = inventoryManager.gameObject.GetComponent<CollisionManager>();
        inventoryVariables = inventoryManager.inventoryVariables;
        craftingPanel.Hide(true);
        repairMenu.Hide(true);
    }

    // Update is called once per frame
    void Update()
    {
        PanelManager();
        if (!repairMenu.gameObject.activeInHierarchy)
        {
            checkIfPlayerInsideBarriersAndPressedButton();
        }
    }
    public void AquirePick()
    {
        inventoryVariables.acuiredPickaxe = true;
    }
    public void closeButton()
    {
        pressed = !pressed;
    }
    
    public void AquireAxe()
    {
        inventoryVariables.acuiredAxe = true;
    }
    private void checkIfPlayerInsideBarriersAndPressedButton()
    {
        if (collisionManager.insideCraftsColliders)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                pressed = !pressed;
            }
        }
        else
        {
                pressed = false;
        }
    }
    void PanelManager()
    {
        if (pressed)
        {
            craftingPanel.Show();
        }
        else 
        {
            craftingPanel.Hide();
        }
    }
}
