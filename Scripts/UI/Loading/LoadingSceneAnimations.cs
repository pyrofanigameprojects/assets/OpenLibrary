﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSceneAnimations : MonoBehaviour
{
    private Animator TransisionAnimator;
    // Start is called before the first frame update
    void Start()
    {
        TransisionAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TransitionAnimation()
    {
        TransisionAnimator.SetTrigger("LoadScene");
    }
}
