﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="UiData",menuName ="UiSettings/UiData",order =4)]
public class UiData : ScriptableObject
{   [Range(0,1)]
    public float Volume;
    [Range(70,90)]
    public float fov;
    public bool[] characters;

}
