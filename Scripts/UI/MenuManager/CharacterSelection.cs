﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CharacterSelection : MonoBehaviour
{   [Header("Statistics")]
    public TextMeshProUGUI reloadTMP;
    public TextMeshProUGUI speedTMP;
    public TextMeshProUGUI healthTMP;
    public TextMeshProUGUI oxygenTMP;
    public TextMeshProUGUI rangeTMP;
    [Header("Skills")]
    public TextMeshProUGUI FlashLight;
    public TextMeshProUGUI Ammo;
    [Header("EnableDisableAccording to characters containing inv")]
    public GameObject[] equipment;
    [Header("StoresData")]
    public UiData uiData;
    [Header("ScriptableObjectsOfPlayers")]
    public CharStats[] charsStats;
    [Header("InventoryOfCurrentCharacter")]
    public InventoryVariables[] inventoryVariables;
    [Header("CharactersWeaponsSprite")]
    public Sprite[] charactersWeapons;
    // Start is called before the first frame update
    void Start()
    {
        SelectCharacter(0);
        StatsDisplay("Laika");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
  public void StatsDisplay(string charactersName)
    {
        SkillsShowcaseManager(charactersName);
        EquipmentShowcaseManager(charactersName);

        
    }
    public void SelectCharacter(int characterIndex)
    {

   for (int i = 0; i < uiData.characters.Length; i++)
        {
            uiData.characters[i] = false;
        }
        uiData.characters[characterIndex] = true;
        equipment[0].GetComponent<Image>().sprite=charactersWeapons[characterIndex];

    }
    private void SkillsShowcaseManager(string charactersName)
    {
        foreach (CharStats charStats in charsStats)
        {
            if (charStats.name.Contains(charactersName))
            {
                healthTMP.text = "Blood Bag: " + Mathf.RoundToInt(charStats.health).ToString() + " ml";
                oxygenTMP.text = "Oxygen Tank: " + Mathf.RoundToInt(charStats.oxygen / 3).ToString() + " l";
                speedTMP.text = "Velocity: " + Mathf.RoundToInt(charStats.speed + 45 * charStats.speed).ToString() + " m/s";
                rangeTMP.text = "Range: " + Mathf.RoundToInt(charStats.attackRange) + " m";
                if (charStats.ranged)
                {
                    reloadTMP.text = "Firing Rate: " + (1 / charStats.reloadTime).ToString("0.00") + " B/s";

                }
                else
                {
                    reloadTMP.text = "";
                    Ammo.text = "";
                }
            }
            if (charactersName.Contains("BillHill211"))
            {
                oxygenTMP.gameObject.SetActive(false);
            }
            else
            {
                oxygenTMP.gameObject.SetActive(true);

            }
        }
    }
    private void EquipmentShowcaseManager(string charactersName)
    {
        foreach (InventoryVariables inv in inventoryVariables)
        {
            if (charactersName.Contains("Bill"))
            {
                FlashLight.text = "NightVision";
                if (inv.name.Contains("Bounty"))
                {
                    Ammo.text=("Ammunition: " + inv.ammo);
                }
                equipment[2].SetActive(false);
                equipment[1].SetActive(false);
            }
            else
            {
                FlashLight.text = "Flashlight";

            }
            if (inv.name.Contains(charactersName))
            {
                Ammo.text = "Ammunition: " + inv.ammo;
                if (inv.acuiredPickaxe)
                {
                    equipment[2].SetActive(true);
                }
                else
                {
                    equipment[2].SetActive(false);


                }
                if (inv.acuiredAxe)
                {
                    equipment[1].SetActive(true);
                }
                else
                {
                    equipment[1].SetActive(false);
                }
            }
            if (inv.name.Contains("Laika"))
            {
                Ammo.text = "";
            }

        }
    }



}
