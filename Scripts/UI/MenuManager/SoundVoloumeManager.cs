﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundVoloumeManager : MonoBehaviour
{
    private AudioSource[] audioSources;
    private Slider voloumeSlider;
    public UiData uiData;
    private void Awake()
    {

    }
    // Start is called before the first frame update

    void Start()
    { 
            audioSources = Resources.FindObjectsOfTypeAll<AudioSource>();
        if (GetComponent<Slider>() != null)
        {
            voloumeSlider = GetComponent<Slider>();
            voloumeSlider.value = uiData.Volume;
        }

        foreach (AudioSource audio in audioSources)
        {
            audio.volume = uiData.Volume;
        }
    }

    // Update is called once per frame
    void Update()
    {
        SliderSetVoloume();
    }
    public void SliderSetVoloume()
    {   if (GetComponent<Slider>() != null)
        {
            uiData.Volume = voloumeSlider.value;
        }
        foreach (AudioSource audio in audioSources)
        {
            audio.volume = uiData.Volume;

        }
    }
  


}
