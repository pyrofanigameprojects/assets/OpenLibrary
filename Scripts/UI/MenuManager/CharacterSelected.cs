﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelected : MonoBehaviour
{
    public UiData uiData;
    public GameObject[] prefabsCharacters;
    public Transform SpawnPoint;
    // Start is called before the first frame update
    private void Awake()
    {
        Vector3 spawnPoint = new Vector3(SpawnPoint.position.x, 0, SpawnPoint.position.z);
        for (int i = 0; i < uiData.characters.Length; i++)
        {
            prefabsCharacters[i].SetActive(false);
            if (uiData.characters[i])
            {
                GameObject player=  Instantiate(prefabsCharacters[i],spawnPoint,Quaternion.identity);
                player.SetActive(true);
            }
        }
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
