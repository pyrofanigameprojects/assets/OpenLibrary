﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RepairShip : MonoBehaviour
{   [Header("TextsOfRepair")]
    public TextMeshProUGUI woodText;
    public TextMeshProUGUI crystalText;
    public TextMeshProUGUI  oreText;
    public TextMeshProUGUI fuelText;
    [Header("TextsOfObjective")]
    public TextMeshProUGUI woodTextObjective;
    public TextMeshProUGUI crystalTextObjective;
    public TextMeshProUGUI oreTextObjective;
    public TextMeshProUGUI fuelTextObjective;
    private int evenableWood, evenableCrystal, evenableFuel, evenableOre;
    private SpawnLoot[] spawnLoots;
    private inventoryManager inventoryManager;
    private WinningLosePanel winningLose;
    // Start is called before the first frame update
    private void Awake()
    {
        spawnLoots = GameObject.FindObjectsOfType<SpawnLoot>(true);

    }
    void Start()
    {
        inventoryManager = GameObject.FindObjectOfType<inventoryManager>();
        winningLose = GetComponent<WinningLosePanel>();
        foreach(SpawnLoot spawnLoot in spawnLoots)
        {
            if (spawnLoot.loot[0].name.Contains("Wood"))
            {
                evenableWood += spawnLoot.minNumber;
            }
            if (spawnLoot.loot[0].name.Contains("Crystal"))
            {
                evenableCrystal += spawnLoot.minNumber;

            }
            if (spawnLoot.loot[0].name.Contains("Fuel"))
            {
                evenableFuel += spawnLoot.minNumber;

            }
            if (spawnLoot.loot[0].name.Contains("Ore"))
            {
                evenableOre += spawnLoot.minNumber;

            }
            displayText(woodTextObjective, inventoryManager.inventoryVariables.wood, evenableWood / 4);
            displayText(crystalTextObjective, inventoryManager.inventoryVariables.crystals, evenableCrystal / 3);
            displayText(fuelTextObjective, inventoryManager.inventoryVariables.fuel, evenableFuel / 3);
            displayText(oreTextObjective, inventoryManager.inventoryVariables.metal, evenableOre / 4);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        displayText(woodText, inventoryManager.inventoryVariables.wood, evenableWood / 4);
        displayText(crystalText, inventoryManager.inventoryVariables.crystals, evenableCrystal / 3);
        displayText(fuelText, inventoryManager.inventoryVariables.fuel, evenableFuel / 3);
        displayText(oreText, inventoryManager.inventoryVariables.metal, evenableOre / 4);
    }
    private void displayText(TextMeshProUGUI textMeshPro,int resourceObtained,int maxResourcetoGather)
    {
        textMeshPro.text = resourceObtained + "/" + maxResourcetoGather;
    }
    public void RepairShipAction()
    {
        if (EnoughFuelAndOre() && EnoughWoodAndCrystals())
        {         
            winningLose.Won();
            RepairShipActionSubtractValues();
        }
    }
    private void RepairShipActionSubtractValues()
    {
        inventoryManager.inventoryVariables.fuel -= evenableFuel / 3;
        inventoryManager.inventoryVariables.metal -= evenableOre / 4;
        inventoryManager.inventoryVariables.crystals -= evenableCrystal / 3;
        inventoryManager.inventoryVariables.wood -= evenableWood / 4;
    }
    private bool EnoughWoodAndCrystals()
    {
        return inventoryManager.inventoryVariables.wood >= evenableWood / 4 && inventoryManager.inventoryVariables.crystals >= evenableCrystal / 4;
    } 
    private bool EnoughFuelAndOre()
    {
        return inventoryManager.inventoryVariables.fuel >= evenableFuel / 3 && inventoryManager.inventoryVariables.metal >= evenableOre / 3;
    }

}
