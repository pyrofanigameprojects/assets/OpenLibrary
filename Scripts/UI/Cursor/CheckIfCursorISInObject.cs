﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckIfCursorISInObject : MonoBehaviour
{
    private CursorInteractionScript cursorInteractionScript;
    // Start is called before the first frame update
    void Start()
    {
        cursorInteractionScript= GameObject.FindObjectOfType<CursorInteractionScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseOver()
    {
        cursorInteractionScript.tagString = this.gameObject.tag;
    }
    private void OnMouseExit()
    {
        cursorInteractionScript.tagString = "";
    }
}
