﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CursorInteractionScript : MonoBehaviour
{
    private string currentTag;
    [HideInInspector]
    public string tagString;
    [Header("Teaxtures Of Pointers State")]
    public Texture2D  allyCursor;
    public Texture2D  hostilCursor;
    public Texture2D defaultCursor;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(defaultCursor, new Vector2(0, 0), CursorMode.Auto);
    }

    // Update is called once per frame
    void Update()
    {   if (currentTag != tagString)
        {
            changeCursorIcons();
            currentTag = tagString;

        }
    }



    private void changeCursorIcons()
    {
        if (Time.timeScale == 0)
        {
            Cursor.SetCursor(defaultCursor, new Vector2(0, 0), CursorMode.Auto);
        }
       else if (this.tagString.Contains("enemy"))
        {
            Cursor.SetCursor(hostilCursor,new Vector2(0,0),CursorMode.Auto);
        }
        else if (this.tagString.Contains("Harvestable"))
        {
            Cursor.SetCursor(allyCursor, new Vector2(0, 0), CursorMode.Auto);
        }
        else
        {
            Cursor.SetCursor(defaultCursor, new Vector2(0, 0), CursorMode.Auto);
        }

    }

    




}
