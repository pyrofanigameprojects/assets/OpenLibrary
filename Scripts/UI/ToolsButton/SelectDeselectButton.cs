﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectDeselectButton : MonoBehaviour
{
    public Color highlightedColor;
    private ButtonActivate buttonActivate;
    [Header("Sprite to change on selected button/Deselected")]
    public Sprite higighlitedImage;
    public Sprite initialImage;
    [HideInInspector]
    public bool activatedMainButton = false, activatedAxeButton, activatedPickButton;
    private Button axeButton, pickButton, mainButton;//public mono gia debug
    private AnimationsPlayer animationsPlayer;
    private inventoryManager inventoryManager;
    private InventoryVariables inventoryVariables;
    // Start is called before the first frame update
    private void Awake()
    {

    }
    void Start()
    {
        animationsPlayer = GameObject.FindObjectOfType<AnimationsPlayer>();
        inventoryManager = animationsPlayer.GetComponentInParent<inventoryManager>();
        if (animationsPlayer.gameObject.name.Contains("Laika"))
        {
            gameObject.SetActive(false);
        }
        buttonActivate = GetComponent<ButtonActivate>();
        axeButton = buttonActivate.axeButton;
        pickButton = buttonActivate.pickButton;
        mainButton = buttonActivate.mainButton;
        inventoryVariables = inventoryManager.inventoryVariables;
    }
    void FixedUpdate()
    {
        ActivateButton();
    }

    // Update is called once per frame
    void Update()
    {
        if (!animationsPlayer.AttackAnimationIsPlaying()&&!animationsPlayer.HarvestAnimationIsPlaying())
        {
            SelectState();
        }


    }
    private void SelectState()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            activatedMainButton = !activatedMainButton;
            activatedAxeButton = false;
            activatedPickButton = false;
        }
        if (inventoryVariables.acuiredAxe)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                activatedAxeButton = !activatedAxeButton;
                activatedMainButton = false;
                activatedPickButton = false;
            }
        }
       
        if (inventoryVariables.acuiredPickaxe)
        {
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                activatedPickButton = !activatedPickButton;
                activatedMainButton = false;
                activatedAxeButton = false;

            }
        }
      
    }
    private void ActivateButton()
    {
        CheckWhichImageToActivate(activatedAxeButton, axeButton);
        CheckWhichImageToActivate(activatedMainButton, mainButton);
        CheckWhichImageToActivate(activatedPickButton, pickButton);
    }
    private void CheckWhichImageToActivate(bool activate,Button button)
    {
        if (activate)
        {
            ChangeImageSelectedOrNot(button, higighlitedImage, highlightedColor);


        }
        else
        {
            ChangeImageSelectedOrNot(button, initialImage, new Color(1, 1, 1));
        }
    }
    private void ChangeImageSelectedOrNot(Button button, Sprite sprite, Color color)
    {   if (button != null)
        {
            button.image.sprite = sprite;
            button.image.color = color;
        }


    }


}
