﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponsImageInitializer : MonoBehaviour
{
    [Header("weapon image to take on selected character")]
    public Sprite racoonWeapon;
    public Sprite BillHillWeapon;
    public Sprite MadScientistWeapon;
    private Image weaponsImage,ammoImage;
    private AnimationsPlayer animationsPlayer;

    // Start is called before the first frame update
    void Start()
    {
        animationsPlayer = GameObject.FindObjectOfType<AnimationsPlayer>();

    }

    // Update is called once per frame
    void Update()
    {   if (weaponsImage == null)
        {
            weaponsImage = this.GetComponent<ButtonActivate>().mainButton.gameObject.transform.GetChild(0).GetComponent<Image>();
            ManageWeaponImageAccordingToActivesCharacterName();
        }
        else
        {
            GetComponent<WeaponsImageInitializer>().enabled = false;
        }
    }
    private void ManageWeaponImageAccordingToActivesCharacterName()
    {
        if (animationsPlayer.gameObject.name.Contains("Mad"))
        {
            weaponsImage.sprite = MadScientistWeapon;
        }
        if (animationsPlayer.gameObject.name.Contains("Jack"))
        {
            weaponsImage.sprite = racoonWeapon;
        }
        if (animationsPlayer.gameObject.name.Contains("Bounty"))
        {
            weaponsImage.sprite = BillHillWeapon;
        }
    }
}
