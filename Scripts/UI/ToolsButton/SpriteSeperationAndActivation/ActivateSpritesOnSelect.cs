﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SeperationAndCollectionOfCharactersSprites))]
public class ActivateSpritesOnSelect : MonoBehaviour
{
    private SelectDeselectButton selectDeselect;
    private SeperationAndCollectionOfCharactersSprites seperation;
    private List<GameObject> carryingAxe, carryPickAxe, bothAxe, bothPickaxe;
    private List<GameObject> carryingGun, bothGun;
    private bool toolEfectInUse;
    private void Awake()
    {

    }
    // Start is called before the first frame update
    private void Start()
    {
        selectDeselect =GameObject.FindObjectOfType<SelectDeselectButton>();
        if (GetComponent<ToolEffectManager>() != null)
        {
            toolEfectInUse = true;
        }
        seperation = GetComponent<SeperationAndCollectionOfCharactersSprites>();
        carryingAxe = seperation.carryingAxe;
        carryPickAxe = seperation.carryPickAxe;
        bothAxe = seperation.bothAxe;
        bothPickaxe = seperation.bothPickaxe;
        bothGun = seperation.bothGun;
        carryingGun = seperation.carryGun;
        
    }
    void FixedUpdate()
    {
        actiivateDeactivateSprites();
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void actiivateDeactivateSprites()
    {
        if (selectDeselect.activatedMainButton)
        {
            ActivateDeactivateNeededSprites(carryingGun, true);
            if (bothGun.Count > 0)
            {
                ActivateDeactivateNeededSprites(bothGun, true);

            }
        }
        else
        {
            ActivateDeactivateNeededSprites(bothGun, false);
            ActivateDeactivateNeededSprites(carryingGun, false);

        }
        if (selectDeselect.activatedAxeButton)
        {
            ActivateDeactivateNeededSprites(carryingAxe, true);
            if (bothAxe.Count>0)
            {
                ActivateDeactivateNeededSprites(bothAxe, true);

            }


        }
        else
        {
            ActivateDeactivateNeededSprites(carryingAxe, false);
            ActivateDeactivateNeededSprites(bothAxe, false);

        }
        if (selectDeselect.activatedPickButton )
        {
            ActivateDeactivateNeededSprites(carryPickAxe, true);
            if (bothPickaxe.Count>0)
            {
                ActivateDeactivateNeededSprites(bothPickaxe, true);

            }


        }
        else
        {
            ActivateDeactivateNeededSprites(carryPickAxe, false);
            ActivateDeactivateNeededSprites(bothPickaxe, false);
        }
    }
    protected virtual void ActivateDeactivateNeededSprites(List<GameObject> listToActivate, bool activate)
    //ama dn theloyme to effe afinoume mono to gameobject.setactive
    {

        foreach (GameObject gameObject in listToActivate)
        {  
            if (toolEfectInUse)
            {
                ToolEfectInUseDeactivation(gameObject, activate);
            }
            else
            {
                gameObject.SetActive(activate);
            }
        }
    }
    private void ToolEfectInUseDeactivation(GameObject gameObject,bool activate)
    {
        if (activate == true)
        {
            gameObject.SetActive(activate);

        }
        else
        {
            Material material = gameObject.GetComponent<SpriteRenderer>().material;
            if (material.GetFloat("_Fade") <= 0)
            {
                gameObject.SetActive(activate);
            }
        }
    }


}
