﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//attachToParentOfPlayersSprites
public class SeperationAndCollectionOfCharactersSprites : MonoBehaviour
{
    private Transform[] childsOfGameObject;
    [HideInInspector]
    public List<GameObject> carryingAxe, useAxe, bothAxe, tools,axesAll;
    [HideInInspector]
    public List<GameObject> carryPickAxe, usePickAxe, bothPickaxe, picksAll;
    [HideInInspector]
    public List<GameObject> carryGun, useGun, bothGun, gunAll;
    private void Awake()
    {
        childsOfGameObject = GetComponentsInChildren<Transform>(true);
        SeperateObjects(axesAll, tools, "Axe");
        SeperateObjects(picksAll, tools, "Pick");
        GetGuns(gunAll, "Gun");//μπορει να δυμιουργισει σφαλμα θα δειξει
        SeperateByUse(gunAll, useGun, carryGun, bothGun);
        SeperateByUse(axesAll, useAxe, carryingAxe, bothAxe);
        SeperateByUse(picksAll, usePickAxe, carryPickAxe, bothPickaxe);

    }
    private void Start()
    {
        if (haveAllToolsBoth()&&bothGun.Count>0)
        {
            GetComponent<HarvestSpriteActivation>().enabled=false;
        }
        else
        {
            GetComponent<HarvestSpriteActivation>().enabled = true;
        }
    }
    private void SeperateByUse(List<GameObject> listToSeperate, List<GameObject> Use, List<GameObject> Carrying, List<GameObject> both)
    {
        foreach (GameObject gameObject in listToSeperate)
        {

            if (gameObject.name.Contains("Carry") && gameObject.gameObject.name.Contains("Use"))
            {
                both.Add(gameObject);
            }
            else if (gameObject.name.Contains("Use"))
            {
                Use.Add(gameObject);
            }
            else if (gameObject.name.Contains("Carry"))
            {
                Carrying.Add(gameObject);
            }

        }
    }
    private void SeperateObjects(List<GameObject> listToAdd, List<GameObject> generalToolsList, string objectType)
    {
        foreach (Transform transform in childsOfGameObject)
        {
            if (transform.name.Contains(objectType))
            {
                listToAdd.Add(transform.gameObject);
                generalToolsList.Add(transform.gameObject);
            }
        }

    }
    private void GetGuns(List<GameObject> listToAdd, string objectType)
    {
        foreach (Transform transform in childsOfGameObject)
        {
            if (transform.name.Contains(objectType))
            {
                listToAdd.Add(transform.gameObject);
            }
        }
    }
    bool haveAllToolsBoth()
    {
        return bothPickaxe.Count > 0 && bothAxe.Count > 0;
    }
}
