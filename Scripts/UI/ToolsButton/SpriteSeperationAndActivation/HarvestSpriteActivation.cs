﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarvestSpriteActivation : MonoBehaviour //einai proektasi toy activate deactivate sprites kai leitoyrgei gia sigkekrimenoys xaraktrires
{
    private SeperationAndCollectionOfCharactersSprites seperation;
    private SelectDeselectButton selectDeselectComponenent;
    private List<GameObject> useAxe,usePickaxe,carryAxe,CarryPick;
    private List<GameObject> useGun, carryGun;
    private AnimationsPlayer animationsPlayer;
    // Start is called before the first frame update
    private void Awake()
    {
        this.GetComponent<HarvestSpriteActivation>().enabled = false;
    }
    void Start()
    {
        animationsPlayer = GameObject.FindObjectOfType<AnimationsPlayer>();
        seperation = GameObject.FindObjectOfType<SeperationAndCollectionOfCharactersSprites>();
        selectDeselectComponenent = GameObject.FindObjectOfType<SelectDeselectButton>();
        useAxe = seperation.useAxe;
        usePickaxe = seperation.usePickAxe;
        carryAxe = seperation.carryingAxe;
        CarryPick = seperation.carryPickAxe;
        useGun = seperation.useGun;
        carryGun = seperation.carryGun;
    }
    void FixedUpdate()
    {
  
    }
    // Update is called once per frame
    void Update()
    {
        if (animationsPlayer.HarvestAnimationIsPlaying())
        {
            UseEquipment();
            DeactivateOnUseTools(carryAxe);
            DeactivateOnUseTools(CarryPick);
        }
        else
        {
            DeactivateOnUseTools(useAxe);
            DeactivateOnUseTools(usePickaxe);
        }
        if (animationsPlayer.AttackAnimationIsPlaying())
        {
            UseEquipment();
            DeactivateOnUseTools(carryGun);

        }
        else
        {
            DeactivateOnUseTools(useGun);
        }
    }
    void UseEquipment()
    {
        if (selectDeselectComponenent.activatedMainButton)
        {   foreach(GameObject gun in useGun)
            {
                gun.SetActive(true);
            }
           
        }    
        if (selectDeselectComponenent.activatedAxeButton)
        {
            ToDoOnClick(useAxe, usePickaxe);
        }  
        if (selectDeselectComponenent.activatedPickButton)
        {
            ToDoOnClick(usePickaxe, useAxe);
        }

    }
    private void ToDoOnClick(List<GameObject> listToActivate,List<GameObject> listToDeactive)
    {   
        foreach (GameObject toActive in listToActivate)
        {
            toActive.SetActive(true);
        }       
        foreach (GameObject toDeactivate in listToDeactive)
        {
            toDeactivate.SetActive(false);
        }
    }
    private void DeactivateOnUseTools(List<GameObject> deactivate)
    {
        foreach (GameObject toDeactivate in deactivate)
        {
            toDeactivate.SetActive(false);
        }
    }
}
