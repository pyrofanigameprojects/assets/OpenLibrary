﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonActivate : MonoBehaviour
{
   

    private inventoryManager inventoryManager;
    private Button[] toolButtons;//public mono gia debug
    [HideInInspector]
    public Button mainButton,axeButton, pickButton;

     void Awake()
    {
        toolButtons = GetComponentsInChildren<Button>();
        foreach (Button button in toolButtons)
        {
            if (!button.gameObject.name.Contains("Main"))
            {
                buttonManager(false, button);

            }
            else
            {
                mainButton = button;

            }
            if (button.gameObject.name.Contains("Axe"))
            {
                axeButton = button;
            }
            if (button.gameObject.name.Contains("Pick"))
            {
                pickButton = button;
            }

        }

    }
    // Start is called before the first frame update
    void Start()
    {

        inventoryManager = GameObject.FindObjectOfType<inventoryManager>();

    }

    // Update is called once per frame
    void Update()
    {
        ButtonToolsManager();
    }

    private void ButtonToolsManager()
    {
        if (this.inventoryManager.inventoryVariables.acuiredAxe)
        {
            buttonManager(true, axeButton);

        }
        if (this.inventoryManager.inventoryVariables.acuiredPickaxe)
        {
            buttonManager(true, pickButton);
        }

    }
    private void buttonManager(bool enabled, Button button)
    {
        button.GetComponent<Image>().enabled = enabled;
        button.transform.GetChild(0).gameObject.SetActive(enabled);
        button.GetComponentInChildren<TextMeshProUGUI>().enabled = enabled;
    }


}
