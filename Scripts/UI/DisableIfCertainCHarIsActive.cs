﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableIfCertainCHarIsActive : MonoBehaviour
{
    private AnimationsPlayer animationsPlayer;
    // Start is called before the first frame update
    void Start()
    {
        animationsPlayer = GameObject.FindObjectOfType<AnimationsPlayer>();
        if (animationsPlayer.gameObject.name.Contains("Laika"))
        {
            gameObject.SetActive(false);
        }
        else if(animationsPlayer.gameObject.name.Contains("Mad"))
        {
            gameObject.SetActive(false);

        }
        else
        {
            gameObject.SetActive(true);
        }
        if (gameObject.name.Contains("Oxy"))
        {
            if (animationsPlayer.gameObject.name.Contains("Bounty"))
            {
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
