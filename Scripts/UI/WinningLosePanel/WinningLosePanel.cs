﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.UI;
using UnityEngine.SceneManagement;


public class WinningLosePanel : MonoBehaviour
{
    public UIView winningView,fadeIn,LoseView,mainMenuWinLose;
    private UIButton sceneLoaderButton;
    // Start is called before the first frame update
    void Start()
    {
        winningView.Hide(true);
        LoseView.Hide(true);
        mainMenuWinLose.Hide(true);
    }

    // Update is called once per frame
    void Update()
    {
        //debugMono
    }
    public void Won()
    {
        winningView.Show();
        fadeIn.Show();
    }
    public void Lost()
    {
        LoseView.Show();
        fadeIn.Show();

    }
  
    public void ReturnToMainMenu()
    {
        StartCoroutine(LoadSceneWithDelay(0));
    }
    private void ShowHideUI(UIView uIView,bool showOrHideOnFalse)
    {
        if (showOrHideOnFalse)
        {
            uIView.Show();
        }
        else
        {
            uIView.Hide();
        }
    }
    IEnumerator LoadSceneWithDelay(int buildIndex)
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(buildIndex);
    }
    IEnumerator TimeScaleChangerDelay(int stateOfTimeScale)
    {
        yield return new WaitForSeconds(1.2f);
        Time.timeScale = stateOfTimeScale;
    }
}
