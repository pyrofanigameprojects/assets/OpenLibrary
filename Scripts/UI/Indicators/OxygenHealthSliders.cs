﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OxygenHealthSliders : MonoBehaviour
{
    private Slider[] sliders;
    private CharStatsManager charStatsManager;
    // Start is called before the first frame update
    void Start()
    {
        charStatsManager = GameObject.FindObjectOfType<CharStatsManager>();
        sliders = GetComponentsInChildren<Slider>();
        foreach(Slider sli in sliders)
        {
            if (sli.gameObject.name.Contains("Health"))
            {
                sli.maxValue = charStatsManager.charstats.health;
            }
            else
            {
                if (charStatsManager.GetComponentInChildren<Animator>().gameObject.name.Contains("Bounty"))
                {
                    sli.GetComponentInParent<HorizontalLayoutGroup>().gameObject.SetActive(false);
                }
                sli.maxValue = charStatsManager.charstats.oxygen;
            }
         
        }
    }
    void FixedUpdate()
    {
        variableUpToDate();
    }
    // Update is called once per frame
    void Update()
    {
    }
    void variableUpToDate()
    {
        foreach (Slider sli in sliders)
        {
            if (sli.gameObject.name.Contains("Health"))
            {
                sli.value = charStatsManager.health;
            }
            else
            {
                sli.value = charStatsManager.oxygen;
            }
        }
    }
    public void UpdateMaxValues()
    {
        foreach (Slider sli in sliders)
        {
            if (sli.gameObject.name.Contains("Health"))
            {
                sli.maxValue = charStatsManager.maxHealth;
            }
            else
            {
                sli.maxValue = charStatsManager.maxOxygen;
            }
        }
    }
}
