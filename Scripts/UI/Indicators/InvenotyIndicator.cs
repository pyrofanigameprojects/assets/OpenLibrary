﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InvenotyIndicator : MonoBehaviour
{
    private inventoryManager inventoryManager;
    private LayoutGroup[] layouts;
    public TextMeshProUGUI ammoTmp;
    // Start is called before the first frame update
    void Start()
    {
        layouts = GetComponentsInChildren<LayoutGroup>();
        inventoryManager = GameObject.FindObjectOfType<inventoryManager>();
        textUpdateAccordingToInventory();
    }

    // Update is called once per frame
    void Update()
    {
        textUpdateAccordingToInventory();
    }
    private void textUpdateAccordingToInventory()
    {
        foreach(LayoutGroup layout in layouts)
        {
            ammoUpdater();
            if (layout.gameObject.name.Contains("Crystal"))
            {
                mainFunction(layout, inventoryManager.inventoryVariables.crystals);
            }       
            if (layout.gameObject.name.Contains("Fuel"))
            {
                mainFunction(layout, inventoryManager.inventoryVariables.fuel);

            }
            if (layout.gameObject.name.Contains("Wood"))
            {
                mainFunction(layout, inventoryManager.inventoryVariables.wood);

            }
            if (layout.gameObject.name.Contains("Metal"))
            {
                mainFunction(layout, inventoryManager.inventoryVariables.metal);

            }
        }
    }
    private void mainFunction(LayoutGroup layout,int amount)
    {
        TextMeshProUGUI tmpText = layout.gameObject.GetComponentInChildren<TextMeshProUGUI>();
        tmpText.text = amount.ToString();
    }
    private void ammoUpdater()
    {   if (ammoTmp.isActiveAndEnabled)
        {
        ammoTmp.text = inventoryManager.inventoryVariables.ammo.ToString();
        }

    }
    
}
