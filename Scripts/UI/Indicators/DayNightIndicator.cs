﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DayNightIndicator : MonoBehaviour
{
    private DayNightManager dayNightManager;
    [Header("State Images")]
    public Image dayImg,duskImg,noonImg;
    [Header("Time Indication Images")]
    public Image dayIndic, duskIndic, NoonIndic;
    private TextMeshProUGUI textMeshProUGUI;
    private float secTillNextState,remainingTime;
    
    // Start is called before the first frame update
    void Start()
    {
        dayNightManager = GameObject.FindObjectOfType<DayNightManager>();
        dayIndic.fillAmount = 0;
        duskIndic.fillAmount = 0;
        NoonIndic.fillAmount = 0;
        textMeshProUGUI = GetComponentInChildren<TextMeshProUGUI>();
        dayNightIndicatorManager();
    }

    // Update is called once per frame
    void Update()
    {

            dayNightIndicatorManager();
        
    }
    private void dayNightIndicatorManager()
    {
        remainingTime = dayNightManager.DayNightVariables.maxSecsPerDay - dayNightManager.dayDurationIndicator;
        secTillNextState = dayNightManager.dayDurationIndicator / 2;
        float calculatedRatioForSunrise = 1 / remainingTime;
        float calculatedRatio = 1 / secTillNextState;
        textMeshProUGUI.text = "Day " + dayNightManager.DayNightVariables.currentDay;
        if (dayNightManager.sunrise)
        {
            NoonIndic.fillAmount = 0;
            duskImg.enabled = false;
            noonImg.enabled = false;
            dayIndic.fillAmount += calculatedRatio * Time.deltaTime;
        }
        if (dayNightManager.dusk)
        {
            dayIndic.fillAmount = 0;
            duskImg.enabled = true;
            duskIndic.fillAmount += calculatedRatio * Time.deltaTime;
        }
        if (dayNightManager.nightfall)
        {
            duskIndic.fillAmount = 0;
            noonImg.enabled = true;
            NoonIndic.fillAmount+= calculatedRatioForSunrise * Time.deltaTime;
        }
        
    }
}
