﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.UI;

public class PopUpNottificationManager : MonoBehaviour
{
    private CollisionManager player;
    private bool pressedKey=false;
    public UIView tutorial_Indicator,fadeInOverlay;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<CollisionManager>();

    }

    // Update is called once per frame
    void Update()
    {
        OnBaseEnterShow();
    }
    private void OnBaseEnterShow()
    {
        ManageInput();
        if (!player.outsideBase)
        {   if (pressedKey)
            {
                tutorial_Indicator.Show();
                fadeInOverlay.Show();
            }

        }
        else
        {
            tutorial_Indicator.Hide();
            fadeInOverlay.Hide();
        }

    }
    private void ManageInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            pressedKey = !pressedKey;
        }
    }
    public void HideOnPress()//used By Button
    {
        tutorial_Indicator.Hide();
        fadeInOverlay.Hide();
    }
}
