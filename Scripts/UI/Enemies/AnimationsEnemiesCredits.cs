﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsEnemiesCredits : MonoBehaviour
{
    public bool attack, hit, idle, walk,death;
    public int direction;
    private Animator animator;
    private SpriteRenderer[]spriteRenderers;
    [Range(0,1)]
    public float alphaChange;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        animator.SetInteger("Direction", direction);
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Direction", direction);
        if (attack)
        {
            animator.SetTrigger("Attack");
        }
        if (hit)
        {
            animator.SetTrigger("Hit");
        }
        if (death)
        {
            animator.SetTrigger("Death");
        }
        if (walk)
        {
            animator.SetBool("Walking", true);
        }
        else
        {
            animator.SetBool("Walking", false);

        }
        if (idle)
        {
            animator.SetBool("Idle", true);
        }
        else
        {
            animator.SetBool("Idle", false);
        }
        if (1< animator.GetCurrentAnimatorStateInfo(0).normalizedTime&&!animator.IsInTransition(0))
        {
            if (gameObject.name.Contains("Canvas"))
            {
                gameObject.SetActive(false);
            }
        }
        if (alphaChange != 1)
        {
            foreach (SpriteRenderer sprite in spriteRenderers)
            {
                sprite.color = new Color(sprite.color.r, sprite.color.g,sprite.color.b, alphaChange);

            }
        }
    }
}
