﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaikaDisable : MonoBehaviour
{
    private AnimationsPlayer animationsPlayer;
    // Start is called before the first frame update
    void Start()
    {
        animationsPlayer = GameObject.FindObjectOfType<AnimationsPlayer>();
        if (animationsPlayer.gameObject.name.Contains("Laika"))
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
