﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LosingStatsHealthOxyIndicator : MonoBehaviour
{
    public Image imageToAplyEffectTo;
    private Color initialColor;
    public Color colorToChangeTo;
    private float timer = 0;
    // Start is called before the first frame update
    void Start()
    {   if (colorToChangeTo == Color.black)
            colorToChangeTo = Color.red;
        initialColor = imageToAplyEffectTo.color;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }
    public void FlashEffect()
    {   if (timer > 1f)
        {
            StartCoroutine(flashEffect());
            timer = 0;
        }
    }
    private IEnumerator flashEffect()
    {
        imageToAplyEffectTo.color = colorToChangeTo;
        yield return new WaitForSeconds(0.5f);
        imageToAplyEffectTo.color = initialColor;
    }
}
