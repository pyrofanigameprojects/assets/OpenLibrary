﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRotationForSpritesAtStart : MonoBehaviour
{
    // Start is called before the first frame update
    [Tooltip("45 seems to work fine adjust only if the certain sprite is not displayed correctly")]
    public int spriterXRotation= 45;
    private  GameObject _MainCamer;
    private  Transform objectsTransform;
    private  Transform cameraTransf;

    void Start()
    { 
        objectsTransform = this.gameObject.GetComponent<Transform>();
        _MainCamer = GameObject.FindObjectOfType<Camera>().gameObject;
        cameraTransf = _MainCamer.transform;

    }

    // Update is called once per frame
    void Update()
    {
        objectsTransform.localEulerAngles = new Vector3(spriterXRotation, cameraTransf.rotation.eulerAngles.y, 0);
    }

}
