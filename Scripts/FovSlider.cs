﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class FovSlider : MonoBehaviour
{
    private CinemachineVirtualCamera camerPlayer;
    public UiData uiData;
    private Slider slider;
    // Start is called before the first frame update
    private void Awake()
    {
        slider = GetComponent<Slider>();

    }
    void Start()
    {
        slider.value = uiData.fov;
        camerPlayer = GameObject.FindObjectOfType<CinemachineVirtualCamera>();
        camerPlayer.m_Lens.FieldOfView = uiData.fov;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {

    }
    public void ChangeFov()
    {
        uiData.fov = slider.value;
        if (camerPlayer != null)
        {
            camerPlayer.m_Lens.FieldOfView = uiData.fov;
        }
    }
}
