﻿using ECM.Common;
using ECM.Controllers;
using UnityEngine;


public class MovementOvewright : BaseCharacterController
{   [HideInInspector]
    public Vector3 charactersDirection;
    private bool pressedMiddleMouse = false;
   protected override void UpdateRotation()
    {

    }
    protected override void HandleInput()
    {
        base.HandleInput();
        moveDirection = moveDirection.relativeTo(transform);
        charactersDirection = moveDirection;

    }
    public void StopCharacterMovement(float speedToResetTo)
    {
        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            pressedMiddleMouse = !pressedMiddleMouse;
        }
        if (pressedMiddleMouse)
        { Rigidbody rb = GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezePosition;
        }
        else
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.None | RigidbodyConstraints.FreezeRotation;
        }
    }
    public void StopCharacterOnDemand(bool stopHim)
    {
        if (stopHim)
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezePosition;
        }
        else
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.None | RigidbodyConstraints.FreezeRotation;
        }
    }



}
