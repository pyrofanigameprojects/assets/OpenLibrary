﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsPlayer : MonoBehaviour
{
    private Animator animator;
    private Transform playersTransform;
    private Vector3 charsScale;
    private Vector3 initialScale;
    private Vector3 playerMovement;
    private CharStatsManager charStatsManager;
    private float reloadTime = 0;
    private float timer = 0;
    private SelectDeselectButton selectDeselectButtons;
    private AnimatorClipInfo[] animatorClipInfo;
    private bool movingLeft;

    // Start is called before the first frame update
    void Start()
    {
        selectDeselectButtons = GameObject.FindObjectOfType<SelectDeselectButton>();
        animator = GetComponent<Animator>();
        playersTransform = GetComponent<Transform>();
        if (playersTransform.parent != null)
        {
            initialScale = playersTransform.localScale;
            charsScale = initialScale;
            charStatsManager = GetComponentInParent<CharStatsManager>();
            reloadTime = charStatsManager.charstats.reloadTime;
        }

    }

    // Update is called once per frame
    void Update()
    {   
        timer += Time.deltaTime;
        FlipScaleManager();
        animationsControllBools();
        DisableMovementIfHarvestAnimationIsPlaying();
    }
    private void animationsControllBools()
    {   if (!HarvestAnimationIsPlaying())
        {
            CheckIfRangedAndIfHasAmmo();
        }
        playersTransform.localScale = charsScale;
        playerMovement.x = Input.GetAxisRaw("Horizontal");
        playerMovement.z = Input.GetAxisRaw("Vertical");
        playerMovement = playerMovement.normalized;
        animator.SetFloat("VerticalMovement", playerMovement.z);
        animator.SetFloat("HorizontalMovement", Mathf.Abs(playerMovement.x));
        if (playerMovement.x > 0)
        {
            movingLeft = false;
        }
        else if (playerMovement.x< 0)
        {
            movingLeft = true;
        }


    }
    public void Death()
    {
        animator.SetTrigger("Death");
    }
    private void TriggerAnimations(bool canAtack)
    {
        if (charStatsManager.charstats.ranged)
        {
            if (!IfRangedRdyToAttackAndOnMove())
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    animator.SetTrigger("Harvest");
                    timer = 0;
                }
            }
        }


        if (!charStatsManager.charstats.ranged)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && timer >= reloadTime)
            {
                AttackAnimationPlay();
                timer = 0;
            }
        }


    }
    public void playHarvestAnimations()
    {
        animator.SetTrigger("Harvest");
    }
    public void AttackAnimationPlay()
    {  
            animator.SetTrigger("Attack");
    }  
    public void AttackAnimationPlayNoMovement()
    {
            animator.SetFloat("VerticalMovement", 1);
            animator.SetTrigger("Attack");
    }
    public void HitAnimationsPlay( Color colorToReturnTo)
    {
        animator.SetTrigger("Hit");
        StartCoroutine(colorChanger(colorToReturnTo));
    }
    private void CheckIfRangedAndIfHasAmmo()
    {
        if (!charStatsManager.charstats.ranged)
        {
            TriggerAnimations(false);

        }
        else
        {
            if (GetComponentInParent<inventoryManager>().inventoryVariables.ammo > 0)
            {
                TriggerAnimations(true);
            }
        }
    }
    private string nameOfCurrentClip()
    {
        animatorClipInfo = animator.GetCurrentAnimatorClipInfo(0);
        return animatorClipInfo[0].clip.name;
    }
    private bool IfRangedRdyToAttackAndOnMove()
    {
        return charStatsManager.charstats.ranged && selectDeselectButtons.activatedMainButton;
    }
    public bool HarvestAnimationIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Harvest")
            && animator.GetCurrentAnimatorStateInfo(0).length > animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
    public bool AttackAnimationIsPlaying()
    {
        return nameOfCurrentClip().Contains("Attack")
            && animator.GetCurrentAnimatorStateInfo(0).length > animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
    private void DisableMovementIfHarvestAnimationIsPlaying()
    {
        if (HarvestAnimationIsPlaying())
        {
            GetComponentInParent<MovementOvewright>().StopCharacterOnDemand(true);
        
        }
        else
        {
            GetComponentInParent<MovementOvewright>().StopCharacterOnDemand(false);

        }
    }
    private void FlipScaleManager()
    {
        if (movingLeft)
        {
            charsScale.x = -initialScale.x;
        }
        else
        {
            charsScale.x = initialScale.x;
        }
    }
    IEnumerator colorChanger(Color colorToReturnTo)
    {
        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>(true);
        foreach (SpriteRenderer sprite in sprites)
        {
            sprite.material.SetColor("_ColorBlood", Color.red);
        }
        yield return new WaitForSeconds(0.3f);
        foreach (SpriteRenderer spriteRenderer in sprites)
        {
            spriteRenderer.material.SetColor("_ColorBlood", colorToReturnTo);
        }
    }
    public void ResetColourToWhite()
    {
        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>(true);

        foreach (SpriteRenderer spriteRenderer in sprites)
        {
            spriteRenderer.material.SetColor("_ColorBlood", Color.white);
        }
    }
    public IEnumerator ResetColourWithDelay()
    {
        yield return new WaitForSeconds(1);
        ResetColourToWhite();
    }  
    public IEnumerator changeDirAndAttack()
    {
        yield return new WaitForSeconds(0.01f);
        animator.SetFloat("VerticalMovement", 1);
        animator.SetTrigger("Attack");
    }

}
