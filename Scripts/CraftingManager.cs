﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CraftingManager : MonoBehaviour
{
    //[HideInInspector]
    public int wood, ore, fuel, crystals;
    [Header("CostTexts")]
    [SerializeField]
    private TextMeshProUGUI woodTxt;
    [SerializeField]
    private TextMeshProUGUI crystalsTxt;
    [SerializeField]
    private TextMeshProUGUI fuelTxt;
    [SerializeField]
    private TextMeshProUGUI oreTxt;
    [HideInInspector]
    public InventoryVariables inventoryVariables;
    private CharStatsManager charStatsManager;
    // Start is called before the first frame update

    private void Start()
    {
        charStatsManager = GameObject.FindObjectOfType<CharStatsManager>();
        inventoryVariables = charStatsManager.GetComponent<inventoryManager>().inventoryVariables;
    }


    // Update is called once per frame
    void Update()
    {
        checkIfValueChangedAndChangeText(wood, woodTxt);
        checkIfValueChangedAndChangeText(ore, oreTxt);
        checkIfValueChangedAndChangeText(crystals, crystalsTxt);
        checkIfValueChangedAndChangeText(fuel, fuelTxt);
    }
    private void checkIfValueChangedAndChangeText(int variableToDisplay, TextMeshProUGUI textToApplyTo)
    {

            textToApplyTo.text = variableToDisplay.ToString();

    }

    public void SubtracCost()
    {
        inventoryVariables.wood -= wood;
        inventoryVariables.metal -= ore;
        inventoryVariables.fuel -= fuel;
        inventoryVariables.crystals -= crystals;

    }
    public void UpgradeHealth()
    {
        charStatsManager.maxHealth = 2 * charStatsManager.maxHealth;
    }
    public void UpgradeOxygen()
    {
        charStatsManager.maxOxygen = 2 * charStatsManager.maxOxygen;
    }
    public void CraftPick()
    {
        inventoryVariables.acuiredPickaxe = true;
    }
    public void CraftAxe()
    {
        inventoryVariables.acuiredAxe = true;
    }

}
